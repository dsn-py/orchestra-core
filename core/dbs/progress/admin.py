from django.contrib import admin
from .models import (ProgressCategory, ProgressDiscussion, ProgressCategoryDistinct,
						ProgressReport, ProgressHistory)


@admin.register(ProgressCategory)
class ProgressCategoryAdmin(admin.ModelAdmin):
	search_fields = ('event__display_name__icontains',)
	list_display = ('event', 'display_name', 'created_at' )

@admin.register(ProgressCategoryDistinct)
class ProgressCategoryDistinctAdmin(admin.ModelAdmin):
	list_display = ('event', 'user', 'category',)
	search_fields = ('user__username', 'event__display_name__icontains',)


class ProgressDiscussionInline(admin.TabularInline):
	model = ProgressDiscussion
	list_display = ('user', 'text')
	extras = 0


@admin.register(ProgressReport)
class ProgressReportAdmin(admin.ModelAdmin):
	list_display = ('event', 'user', 'team', 'category', 'created_at')
	inlines = [ProgressDiscussionInline,]
	search_fields = ('user__username', 'event__display_name__icontains',)


@admin.register(ProgressHistory)
class ProgressHistoryAdmin(admin.ModelAdmin):
	list_display = ('user', 'event', 'progress', 'created_at')