from django.db import models
from django.utils.text import slugify
from django.utils import timezone
from django.contrib.auth.models import User
from core.dbs.base.models import BaseModel
from core.dbs.master.models import Event
from core.dbs.activity.models import Activity
from core.dbs.participant.models import Team
from django.utils.text import slugify
from core.libs import storage


class ProgressCategory(BaseModel):
	event = models.ForeignKey(Event, on_delete=models.CASCADE, 
										blank=True, null=True)
	display_name = models.CharField(max_length=32)
	short_name = models.CharField(max_length=32, blank=True, null=True)

	def __str__(self):
		return self.short_name

	def save(self, *args, **kwargs):
		if not self.short_name:
			self.short_name = slugify(self.display_name)
		super().save(*args, **kwargs)


class ProgressCategoryDistinct(models.Model):
	event = models.ForeignKey(Event, on_delete=models.CASCADE)
	user = models.ForeignKey(User, on_delete=models.CASCADE, 
							related_name="progress_category_distinct_user")
	category = models.CharField(max_length=32)

	def __str__(self):
		return "{}|{} : {}".format(self.event.short_name, 
						self.user.username, self.category)

	class Meta:
		unique_together = ("event", "user", "category")


class ProgressHistory(BaseModel):
	PROGRESS_STATUS_CHOICES = (
		("create", "Create Progress"),
		("feedback", "Feedback Progress"),
	)
	obj_created_at = models.DateTimeField(blank=True, null=True)
	event = models.ForeignKey(Event, on_delete=models.CASCADE)
	user = models.ForeignKey(User, on_delete=models.CASCADE, )
	progress = models.ForeignKey("ProgressReport", on_delete=models.CASCADE)
	status = models.CharField(choices=PROGRESS_STATUS_CHOICES, max_length=20)
	object_id = models.BigIntegerField(blank=True, null=True)

	def __str__(self):
		return "{} {}".format(self.object_id, self.status)

	class Meta:
		ordering = ['-obj_created_at',]


class ProgressReport(BaseModel):
	event = models.ForeignKey(Event, on_delete=models.CASCADE)
	user = models.ForeignKey(User, on_delete=models.CASCADE, 
									related_name="progress_report_user")
	team = models.ForeignKey(Team, on_delete=models.CASCADE, blank=True, null=True,
									related_name="progress_response_team")
	referenced_category = models.ForeignKey(ProgressCategory, blank=True, 
									null=True, on_delete=models.CASCADE)
	category = models.CharField(max_length=32, blank=True, null=True)
	grow_number = models.DecimalField(max_digits=20, decimal_places=2)
	grow_percent = models.DecimalField(max_digits=10, decimal_places=2)
	description = models.TextField(blank=True)
	file = models.FileField(storage=storage.STORAGE_TASKLIST,
											blank=True, null=True) 
	mentor = models.ForeignKey(User, on_delete=models.CASCADE, 
							blank=True, null=True, 
							related_name="progress_report_mentor")
	checked_at = models.DateTimeField(blank=True, null=True)
	checked_by = models.ForeignKey(User,on_delete=models.CASCADE, 
							blank=True, null=True,)
	extra = models.TextField(blank=True,)

	def __str__(self):
		return "{}:{}".format(self.event.short_name, self.category)

	def save(self, *args, **kwargs):
		if self.referenced_category:
			self.category = self.referenced_category.short_name
		data, created = ProgressCategoryDistinct.objects.get_or_create(
			event = self.event,
			user = self.user,
			category = self.referenced_category.short_name
		)
		super(ProgressReport, self).save(*args, **kwargs)
		activity = Activity()
		activity.event=self.event
		activity.content_object=self
		activity.created_by=self.user
		activity.model="progress-tracking"
		activity.user=self.mentor
		activity.save()

		history, created = ProgressHistory.objects.get_or_create(
				event=self.event,
				user=self.user,
				created_by=self.user,
				status="create",
				object_id=self.id,
				progress=self)
		history.obj_created_at = self.created_at
		history.save()

	class Meta:

		permissions = [
				("ds_can_view_progress", "Can view progress report (DS)"),
				("ds_can_add_progress", "Can add progress report (DS)"),
				("ds_can_edit_progress", "Can edit progress report (DS)"),
				("ds_can_delete_progress", "Can delete progress report (DS)"),
			]


class ProgressDiscussion(BaseModel):
	"""
	This discussion only between user and checked_by in Progress Report
	"""
	progress = models.ForeignKey(ProgressReport, on_delete=models.CASCADE, related_name='progress_discussion')
	user = models.ForeignKey(User, on_delete=models.CASCADE, 
									related_name="progress_discussion_user")
	text = models.TextField()

	def __str__(self):
		return "{} :: {}".format(self.user.username, self.text)

	def save(self, *args, **kwargs):
		super().save(*args, **kwargs)
		history = ProgressHistory()
		history.event = self.progress.event
		history.created_by = self.created_by
		history.obj_created_at = self.created_at if self.created_at else timezone.now()
		history.status = "feedback"
		history.user = self.user
		history.progress = self.progress
		history.object_id = self.id
		history.save()



