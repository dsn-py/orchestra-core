from django.db import models
from django.utils import timezone
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

from core.dbs.master.models import Event
from core.dbs.base.models import BaseModel
from core.dbs.participant.models import EventGroup as Group, Team, User, Event
from core.libs import storage


class Notification(BaseModel):
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, blank=True, null=True)
    object_id = models.PositiveIntegerField(blank=True, null=True)
    content_object = GenericForeignKey('content_type', 'object_id')
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    users = models.ManyToManyField(User, blank=True)
    title = models.CharField(max_length=256,)
    description = models.CharField(max_length=256, blank=True, null=True)
    status = models.BooleanField(default=False)

    def __str__(self):
        return self.title

def save_notification(obj, creator, event, recipients, title, description=None):
    notification = Notification.objects.filter(
        created_by=creator,
        updated_by=creator,
        event=event,
        title=title,
        description=description,
    )
    if not notification.exists():
        notification = Notification.objects.create(
            created_by=creator,
            updated_by=creator,
            event=event,
            title=title,
            description=description,
        )
        notification.users.add(*recipients)
        notification.content_object = obj
        notification.save()


def get_notifications(event, user, now=timezone.now()):
    data = []
    notifications = Notification.objects.filter(
            event=event,
            users=user,
            # status=0,
            created_at__gte=now,
        ).order_by('content_type')

    for item in notifications:
        # recipients = set()
        obj = item.content_object

        if obj is not None:

            # for user in item.users.all():
            #     recipients.add(user.id)

            payload = {
                "id" : item.id,
                "obj" : obj.__class__.__name__,
                "obj_id" : obj.id,
                "title": item.title,
                "event_id" : item.event_id
                # "recipients" : list(recipients)
            }
        else:
            payload = {
                "id" : item.id,
                "obj": None,
                "obj_id": None,
                "title": None,
                "event_id" : item.event_id
                # "recipients" : None
            }
        data.append(payload)
    return data


def generate_notifications(event, user, now):
    notifications = get_notifications(event, user, now)
    types = Notification.objects.filter(
            event=event,
            users=user,
            # status=0,
            created_at__gte=now,
        ).order_by('content_type').values_list("content_type__model", flat=True).distinct()

    data = dict.fromkeys(types, [])

    for key, value in data.items():
        temp = []
        for item in notifications:
            if item["obj"].lower() == key:
                temp.append(item)
        data[key] = temp

    return data