from django.contrib import admin
from core.libs.django.admin import RawFieldMixins
from .models import Notification

@admin.register(Notification)
class NotificationAdmin(admin.ModelAdmin, RawFieldMixins):
    list_filter = ('event',)
    list_display = ('id', 'title', 'created_at', 'content_type', 'object_id', 'event', 'get_recipients' )
    raw_id_fields = ('created_by', 'updated_by', 'users', 'event')
    list_select_related = True

    def get_recipients(self, obj):
        return "\n".join([p.first_name for p in obj.users.all()])