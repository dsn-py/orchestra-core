import uuid
import time
from datetime import datetime
from django.db import models
from django.conf import settings
from django.utils.text import slugify
from django.contrib.auth.models import User
from core.dbs.base.models import BaseModel
from core.dbs.participant.models import EventGroup, Event


class MessagingSession(BaseModel):
	recipients = models.ManyToManyField(User, related_name="messaging_recipients", )
	code = models.CharField(max_length=256, blank=True, null=True)
	event = models.ForeignKey(Event, on_delete=models.CASCADE)
	group = models.ForeignKey(EventGroup, on_delete=models.CASCADE)
	user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="messaging_user", blank=True, null=True)
	mentor = models.ForeignKey(User, on_delete=models.CASCADE, related_name="messaging_mentor", blank=True, null=True)

	def __str__(self):
		return self.user.username

	def save(self, *args, **kwargs):
		if not self.code:
			code = uuid.uuid4().hex
			while MessagingSession.objects.filter(code=code).exists():
				time.sleep(0.2)
				code = uuid.uuid4().hex
			self.code = code
		super().save(*args, **kwargs) 


class MessagingDetail(BaseModel):
	session = models.ForeignKey(MessagingSession, on_delete=models.CASCADE)
	text = models.TextField()
	read_by = models.ManyToManyField(User, related_name="message_read_by", blank=True)

	def __str__(self):
		return self.created_by.username 