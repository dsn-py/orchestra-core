from django.contrib import admin
from core.libs.django.admin import RawFieldMixins
from .models import MessagingSession, MessagingDetail


class MessagingDetailInline(admin.TabularInline, RawFieldMixins):
    model = MessagingDetail
    raw_id_fields = ('created_by', )
    list_select_related = True
    extra = 0


class MessagingAdmin(admin.ModelAdmin, RawFieldMixins):
    list_display = ('event', 'group', 'created_at', 'user', 'mentor')
    inlines = [MessagingDetailInline,]
    raw_id_fields = ('created_by', 'updated_by', 'recipients')
    list_select_related = True


admin.site.register(MessagingSession, MessagingAdmin)