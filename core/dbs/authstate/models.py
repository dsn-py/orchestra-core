from django.db import models
from django.db.models.signals import post_save
from knox.models import AuthToken
from core.dbs.base.models import BaseModel
from core.dbs.master.models import Event


def save_state(sender, instance, **kwargs):
    if not State.objects.filter(token=instance).exists():
        last_token = AuthToken.objects.filter(user=instance.user).exclude(id=instance.id).last()
        event = last_token.auth_state_token.current_event
        State.objects.create(token=instance, created_by=instance.user, current_event=event)
post_save.connect(save_state, sender=AuthToken)


class State(BaseModel):
    token = models.OneToOneField(AuthToken, related_name='auth_state_token', 
                                                on_delete=models.CASCADE)
    current_event = models.ForeignKey(Event, on_delete=models.CASCADE, 
                                        blank=True, null=True)
    
    def __str__(self):
        return "{} [ {} ]".format(self.token.user.username, self.current_event)