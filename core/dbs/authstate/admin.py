from django.contrib import admin
from .models import State

class StateAdmin(admin.ModelAdmin):
	list_display = ('token', 'current_event')
	search_fields = ('token__user__username',)


admin.site.register(State, StateAdmin)