from datetime import datetime
from django.db import models
from django.conf import settings
from django.utils.text import slugify
from core.libs.moment import to_timestamp
from core.dbs.base.models import BaseModel
from django.contrib.auth.models import User
from core.dbs.master.models import Event, EventPlace
from core.libs.constants.events import ATTEND_TYPE_CHOICES


class Attendee(BaseModel):
	event = models.ForeignKey(Event, on_delete=models.CASCADE, )
	user = models.ForeignKey(User, on_delete=models.CASCADE, 
										related_name="attendee_user")
	place = models.ForeignKey(EventPlace, on_delete=models.CASCADE, blank=True, null=True)
	type = models.CharField(max_length=20, choices=ATTEND_TYPE_CHOICES, default="visitor")
	code = models.CharField(max_length=64, blank=True, null=True)
	rsvp = models.BooleanField(default=False)
	attended_at = models.DateTimeField(blank=True, null=True)
	is_qualified = models.BooleanField(default=False)
	can_login = models.BooleanField(default=False)
	company = models.ForeignKey("company.Company", on_delete=models.SET_NULL, blank=True, null=True)

	def save(self, *args, **kwargs):
		self.code = to_timestamp(datetime.now(), epoch=getattr(settings, 'EPOCH', None))
		super(Attendee, self).save(*args, **kwargs)	

	def __str__(self,):
		return "{} : {} ({})".format(self.user.username, self.event.display_name, self.event_id)

	class Meta:
		permissions = [
			("ds_can_view_attendee", "Can view attendee (DS)"),
			("ds_can_add_attendee", "Can add attendee (DS)"),
			("ds_can_edit_attendee", "Can edit attendee (DS)"),
			("ds_can_delete_attendee", "Can delete attendee (DS)"),
			("ds_can_view_judge", "Can view judge (DS)"),
			("ds_can_add_judge", "Can add judge (DS)"),
			("ds_can_edit_judge", "Can edit judge (DS)"),
			("ds_can_delete_judge", "Can delete judge (DS)"),
		]


class Team(BaseModel):
	"""
	for team based participant
	"""
	event = models.ForeignKey(Event, on_delete=models.CASCADE)
	display_name = models.CharField(max_length=128)
	short_name = models.CharField(max_length=64, blank=True, null=True)
	is_qualified = models.BooleanField(default=False)
	is_assessment_passed = models.BooleanField(default=False)
	assessment_score = models.IntegerField(blank=True, null=True)
	tech_stack	= models.CharField(max_length=255, blank=True, null=True)
	can_login = models.BooleanField(default=False)

	def save(self, *args, **kwargs):
		if self.display_name:
			self.short_name = slugify(self.display_name)
		super(Team, self).save(*args, **kwargs)

	def __str__(self):
		return "{} :: {}".format(self.event.display_name, self.short_name)

	class Meta:
		permissions = [
			("ds_can_view_team", "Can view team (DS)"),
			("ds_can_add_team", "Can add team (DS)"),
			("ds_can_edit_team", "Can edit team (DS)"),
			("ds_can_delete_team", "Can delete team (DS)"),
		]


class TeamMembers(BaseModel):
	team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name='members')
	email = models.EmailField()
	user = models.ForeignKey(User, on_delete=models.CASCADE,
								blank=True, null=True, related_name='participant_teammembers_member') 
	is_active = models.BooleanField(default=False)
	is_leader = models.BooleanField(default=False)

	def __str__(self):
		return "{}{} | active:{}".format(
									self.email,
									"[lead]" if self.is_leader else "",
									 str(self.is_active))


class EventGroup(BaseModel):
	"""
	will be classroom on incubation
	"""
	event = models.ForeignKey(Event, on_delete=models.CASCADE)
	display_name = models.CharField(max_length=64)
	short_name = models.CharField(max_length=64, blank=True, null=True)
	mentor = models.ForeignKey(User, on_delete=models.CASCADE, 
								blank=True, null=True,
								related_name="participant_eventgroup_mentor")
	place = models.ForeignKey(EventPlace, on_delete=models.CASCADE)
	
	def __str__(self,):
		return "{} : {} ({})".format(self.display_name, self.event.display_name, self.event_id)

	def save(self, *args, **kwargs):
		if self.display_name:
			self.short_name = slugify(self.display_name)
		super(EventGroup, self).save(*args, **kwargs)

	class Meta:
		permissions = [
			("ds_can_view_classroom", "Can view classroom (DS)"),
			("ds_can_add_classroom", "Can add classroom (DS)"),
			("ds_can_edit_classroom", "Can edit classroom (DS)"),
			("ds_can_delete_classroom", "Can delete classroom (DS)"),
		]


class GroupMembers(BaseModel):
	"""
	a classroom or a group
	"""
	attendee = models.ForeignKey(Attendee, on_delete=models.CASCADE)
	user = models.ForeignKey(User, on_delete=models.CASCADE, 
										related_name="participant_groupmembers_member")
	group = models.ForeignKey(EventGroup, on_delete=models.CASCADE)
	role_title = models.CharField(blank=True, null=True, max_length=85)
	is_active = models.BooleanField(default=False)
	is_mentor = models.BooleanField(default=False)
	is_supermentor = models.BooleanField(default=False)
	is_leader = models.BooleanField(default=False)

	def __str__(self,):
		return self.user.username

	def save(self, *args, **kwargs):
		super().save(*args, **kwargs)
		if self.is_mentor:
			group = self.group
			group.mentor = self.user
			group.save() 
			

class ContactUs(BaseModel):

	event = models.ForeignKey(Event, on_delete=models.CASCADE)	
	name  = models.CharField(max_length=125)
	email  = models.EmailField()
	subject = models.CharField(blank=True, null=True, max_length=225)
	message = models.TextField()

