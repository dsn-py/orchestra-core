from django.contrib import admin
from .models import Attendee, TeamMembers, EventGroup, GroupMembers, ContactUs


class AttendeeAdmin(admin.ModelAdmin):
	list_display = ('user', 'event', 'place', 'type', 'code', 'rsvp')
	list_filter = ('event', 'type',)
	search_fields = ('user__username',)


class TeamMembersAdmin(admin.ModelAdmin):
	list_display = ('user', 'team', 'team_event', 'is_active', 'is_leader', )
	list_filter = ("team__event",)
	search_fields = ("team__display_name", "user__username",)

	def team_event(self, obj):
		return obj.team.event


class EventGroupAdmin(admin.ModelAdmin):
	list_display = ("display_name", "id", "created_at", "event")
	list_filter = ("event",)


class GroupMembersAdmin(admin.ModelAdmin):
	list_display = ('user', 'group_event', 'group', 'is_mentor', 'is_supermentor')
	list_filter = ('group__event',)
	search_fields = ('user__username', 'user__profile__current_company__display_name')

	def group_event(self, obj):
		return obj.group.event


class ContactUsAdmin(admin.ModelAdmin):
	list_display = ("id", "name", "subject", "message", "created_at", "event")
	list_filter = ("event",)


admin.site.register(Attendee, AttendeeAdmin)
admin.site.register(TeamMembers, TeamMembersAdmin)
admin.site.register(EventGroup, EventGroupAdmin)
admin.site.register(GroupMembers, GroupMembersAdmin)
admin.site.register(ContactUs, ContactUsAdmin)

