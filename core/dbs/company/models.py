from django.db import models
from django.utils.text import slugify
from django.contrib.auth.models import User
from core.dbs.base.models import BaseModel, BaseAddressModel
from core.dbs.master.models import Category, Entity, LegalDocument, Event, BusinessScope, BusinessSector
from core.libs import storage
from core.libs.constants import (COMPANY_TYPE_CHOICES, FUNDING_ROUND_CHOICES,
                                INVOLVEMENT_TYPE_CHOICES, FOUNDER_TYPE_CHOICES)
from core.dbs.applicant.models import ApplicantCompany
 
class Company(BaseAddressModel):
    related_events = models.ManyToManyField(Event, blank=True)
    reference = models.ForeignKey(ApplicantCompany, blank=True, null=True, on_delete=models.SET_NULL)
    type = models.CharField(max_length=32, choices=COMPANY_TYPE_CHOICES, default="startup")
    display_name = models.CharField(max_length=128)
    short_name = models.CharField(max_length=128, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    presentation = models.URLField(max_length=256, blank=True, null=True)
    category = models.ForeignKey(Category, blank=True, null=True, on_delete=models.CASCADE)
    business_sector = models.ForeignKey(BusinessSector, on_delete=models.SET_NULL, blank=True, null=True)
    business_scope = models.ManyToManyField(BusinessScope, blank=True)
    entity = models.ForeignKey(Entity, blank=True, null=True, on_delete=models.CASCADE)
    founded_year = models.CharField(max_length=4, blank=True, null=True)
    round_investment = models.CharField(max_length=200, blank=True, null=True)
    total_founder = models.SmallIntegerField(default=1)
    total_employee = models.SmallIntegerField(default=1)
    facebook = models.URLField(blank=True, null=True)
    twitter = models.URLField(blank=True, null=True)
    instagram = models.URLField(blank=True, null=True)
    website = models.URLField(blank=True, null=True)
    marketplace = models.URLField(blank=True, null=True)
    status = models.TextField(max_length=150, blank=True, null=True)
    logo = models.FileField(storage=storage.STORAGE_COMPANY, 
                                            blank=True, null=True)
    last_month_revenue = models.BigIntegerField(default=0, blank=True, null=True)
    monthly_active_users = models.IntegerField(default=0)

    def __str__(self,):
        return self.short_name

    def save(self, *args, **kwargs):
        if self.display_name:
            self.short_name = slugify(self.display_name)
        super(Company, self).save(*args, **kwargs)


class CompanyFounder(BaseModel):
    company = models.ForeignKey(Company, blank=True, null=True, on_delete=models.CASCADE)
    full_name = models.CharField(max_length=128)
    is_leader = models.BooleanField(default=False)
    type = models.CharField(max_length=20, choices=FOUNDER_TYPE_CHOICES)
    position = models.CharField(max_length=40)
    involvement = models.CharField(max_length=20, choices=INVOLVEMENT_TYPE_CHOICES)
    founder_experiences = models.TextField(blank=True, null=True, 
                                                help_text="Experiences in business sector")
    latest_education = models.CharField(max_length=128, blank=True, null=True)

    def __str__(self):
        return "{} | {}".format(self.company.display_name, self.full_name)


class Product(BaseModel):
    """
    PRODUCT RELATED TO STARTUP. FOR INSTANCE: "MATCHBOX" IN YUNA 
    """
    company = models.ForeignKey(Company, blank=True, null=True, on_delete=models.CASCADE)
    display_name = models.CharField(max_length=128)
    short_name = models.CharField(max_length=128, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    photo = models.ImageField(max_length=300, blank=True, null=True,
                                    storage=storage.STORAGE_PRODUCT)

    def __str__(self,):
        return self.short_name

    def save(self, *args, **kwargs):
        if self.display_name:
            self.short_name = slugify(self.display_name)
        super(Product, self).save(*args, **kwargs)


class CompanyLegalDocument(BaseModel):
    """
    SIUPP TDP ETC
    """
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    legal = models.ForeignKey(LegalDocument, blank=True, null=True, on_delete=models.CASCADE)
    legal_other = models.CharField(max_length=32, blank=True, null=True)

    def __str__(self,):
        return "{}|{}".format(self.company.short_name, self.legal.short_name)


class Investment(BaseModel):
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    investor = models.CharField(max_length=100, blank=True)
    type = models.CharField(max_length=40, choices=FUNDING_ROUND_CHOICES)
    amount = models.DecimalField(decimal_places=2, max_digits=20)
    currency = models.CharField(max_length=3, default="IDR")
    investment_date = models.DateField()

    def __str__(self,):
        return self.title

    def save(self, *args, **kwargs):
        if not self.title:
            investor = self.investor
            startup = self.company.short_name
            investment_round = self.type
            self.title = "{investor}|{startup}|{round}".format(
                    investor=investor,
                    startup=startup,
                    round=investment_round,
                )
        super(Investment, self).save(*args, **kwargs)

