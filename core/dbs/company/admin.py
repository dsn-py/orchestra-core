from django.contrib import admin
from .models import (Company, CompanyFounder, CompanyLegalDocument,
												Product, Investment)

class LegalInline(admin.TabularInline):
	model = CompanyLegalDocument
	extras = 0


class CompanyAdmin(admin.ModelAdmin):
	list_display = ('created_by', 'display_name', 'founded_year', 'city')
	search_fields = ('display_name__icontains',)
	inlines = [LegalInline,]


class ProductAdmin(admin.ModelAdmin):
	list_display = ('created_by', 'company', 'display_name')
	search_fields = ('company__display_name__icontains', 'display_name__icontains')


class CompanyFounderAdmin(admin.ModelAdmin):
	list_display = ('company', 'full_name', 'position')
	search_fields = ('company__display_name__icontains', 'full_name__icontains')


class InvestmentAdmin(admin.ModelAdmin):
	list_display = ('company', 'investor', 'type', 'investment_date')
	search_fields = ('company__display_name__icontains', 'investor__icontains')


admin.site.register(Company, CompanyAdmin)
admin.site.register(CompanyFounder, CompanyFounderAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Investment, InvestmentAdmin)