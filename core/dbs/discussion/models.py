from django.db import models
from django.contrib.auth.models import User
from core.libs import storage
from core.dbs.base.models import BaseModel
from core.libs.constants import FORUM_SCOPE_CHOICES
from core.dbs.master.models import Event
from core.dbs.participant.models import EventGroup



class Forum(BaseModel): 
	event = models.ForeignKey(Event, on_delete=models.CASCADE)
	user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="forum_user")
	partner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="forum_partner", blank=True, null=True)
	followers = models.ManyToManyField(User, blank=True)
	scope = models.SmallIntegerField(default=1, choices=FORUM_SCOPE_CHOICES)
	group = models.ForeignKey(EventGroup, on_delete=models.CASCADE, blank=True, null=True)
	title = models.CharField(max_length=200)
	text = models.TextField(blank=True)
	drive_url = models.URLField(blank=True, null=True)
	file = models.FileField(storage=storage.STORAGE_FORUM, blank=True, null=True)
	reference_url = models.URLField(blank=True, null=True)
	comments = models.IntegerField(default=0)

	def __str__(self,):
		return "By {} ({})".format(self.user.username, self.created_at)

	class Meta:

		permissions = [
			("ds_can_view_discussion", "Can view discussion (DS)"),
			("ds_can_add_discussion", "Can add discussion (DS)"),
			("ds_can_edit_discussion", "Can edit discussion (DS)"),
			("ds_can_delete_discussion", "Can delete discussion (DS)"),
		]


class Comments(BaseModel):
	forum = models.ForeignKey(Forum, on_delete=models.CASCADE, related_name="discussion_comments_forum")
	user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="comment_user")
	mentions = models.ManyToManyField(User, blank=True, )
	comment = models.TextField(blank=True)

	def __str__(self,):
		return "By {} ({})".format(self.user.username, self.created_at)

	def save(self,):
		super().save()
		self.forum.comments = models.F('comments') + 1
		self.forum.save()

	class Meta:

		permissions = [
			("ds_can_view_comment", "Can view comment (DS)"),
			("ds_can_add_comment", "Can add comment (DS)"),
			("ds_can_edit_comment", "Can edit comment (DS)"),
			("ds_can_delete_comment", "Can delete comment (DS)"),
		]