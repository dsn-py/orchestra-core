from django.contrib import admin
from .models import Forum, Comments


class CommentInline(admin.TabularInline):
	model = Comments
	fields = ('created_by', 'user', 'comment', 'id')
	extras = 0


@admin.register(Forum)
class ForumAdmin(admin.ModelAdmin):
	list_display = ('title', 'created_at', 'event', 'created_by')
	list_search = ('title__icontains', 'created_by__username__istartswith')
	inlines = [CommentInline,]