from django.db import models
from core.dbs.base.models import BaseModel
from core.dbs.master.models import Event
from core.dbs.participant.models import (EventGroup, GroupMembers,)


class CollaborationProject(BaseModel):
	event = models.ForeignKey(Event, on_delete=models.CASCADE)
	group = models.ForeignKey(EventGroup, on_delete=models.CASCADE)
	lead_by = models.ForeignKey(GroupMembers, on_delete=models.CASCADE)
	display_name = models.CharField(max_length=150)
	short_name = models.CharField(max_length=150, blank=True, null=True)
	goals = models.TextField(blank=True)
	start_at = models.DateTimeField()
	end_at = models.DateTimeField()
	is_active = models.BooleanField(default=True)

	def __str__(self):
		return self.short_name


class ProjectMember(BaseModel):
	project = models.ForeignKey(CollaborationProject, on_delete=models.CASCADE)
	group_member = models.ForeignKey(GroupMembers, on_delete=models.CASCADE)

	def __str__(self):
		"{}::{}".format(self.project.short_name, self.member.user.username)


class ProjectTask(BaseModel):
	TASK_STATUS_CHOICES = (
			('todo', 'To Do'),
			('on-progress', 'On Progress'),
			('on-review', 'On Review'),
			('done', 'Done'),
			('pending', 'Pending'),
		)
	project = models.ForeignKey(CollaborationProject, on_delete=models.CASCADE)
	project_member = models.ForeignKey(ProjectMember, on_delete=models.CASCADE)
	name = models.CharField(max_length=200,)
	description = models.TextField(blank=True)
	due_date = models.DateField(blank=True, null=True)
	status = models.CharField(max_length=30, choices=TASK_STATUS_CHOICES, default="todo")

	def __str__(self):
		return self.name


class TaskHistory(BaseModel):
	task = models.ForeignKey(ProjectTask, on_delete=models.CASCADE)
	status = models.CharField(max_length=30, choices=ProjectTask.TASK_STATUS_CHOICES)

	def __str__(self):
		"{} [ {} ]".format(self.task.name, self.status)