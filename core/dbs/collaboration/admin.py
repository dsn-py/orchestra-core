from django.contrib import admin
from .models import (CollaborationProject, ProjectMember, ProjectTask,
						TaskHistory)


class TaskHistoryInline(admin.TabularInline):
	model = TaskHistory
	extra = 0


class MemberInline(admin.TabularInline):
	model = ProjectMember
	extra = 1


class ProjectTaskInline(admin.TabularInline):
	model = ProjectTask
	extra = 0


@admin.register(CollaborationProject)
class CollabProjectAdmin(admin.ModelAdmin):
	list_display = ('short_name', 'start_at', 'end_at', 'is_active')
	inlines = [MemberInline, ]


@admin.register(ProjectMember)
class ProjectMemberAdmin(admin.ModelAdmin):
	list_display = ('project', 'group_member')
	inlines = [ProjectTaskInline,]


@admin.register(ProjectTask)
class TaskAdmin(admin.ModelAdmin):
	list_display = ('project_member', 'name', 'due_date', )
	inlines = [TaskHistoryInline,]
