from django.conf import settings


def get_question_count(event=None):
	
	question_count =  getattr(settings, "ASSESSMENT_QUESTION_COUNT", 5)

	if event:
		question_count = event.question_count
	return question_count


def get_passed_score(event=None):
	"""
	the default settings on the apps is passing score 3 from 5 questions

	You can modify it to custom and can hit database for custom logic
	"""
	passed_score =  getattr(settings, "ASSESSMENT_PASSING_SCORE", 3)

	if event:
		passed_score = event.passed_score
	return passed_score


def get_try_again_hours(event=None):
	
	try_again =  getattr(settings, "ASSESSMENT_TRY_AGAIN_HOURS", 48)

	if event:
		try_again = event.try_again_hours
	return try_again