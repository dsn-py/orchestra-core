from django.contrib import admin
from .models import (Category, LegalDocument, Entity, BusinessScope, BusinessSector, Event, EventFAQ, Quote,
					Geocode, AspectGroup, Aspect, TournamentRound, EventPlace, Sponsor)


class CategoryAdmin(admin.ModelAdmin):
	list_display = ('id', 'short_name', 'display_name')

class LegalDocumentAdmin(admin.ModelAdmin):
	list_display = ('id', 'short_name', 'display_name')

class EntityAdmin(admin.ModelAdmin):
	list_display = ('id', 'short_name', 'display_name')

class EntityAdmin(admin.ModelAdmin):
	list_display = ('id', 'short_name', 'display_name')

class BusinessScopeAdmin(admin.ModelAdmin):
	list_display = ('id', 'short_name', 'display_name')

class BusinessSectorAdmin(admin.ModelAdmin):
	list_display = ('id', 'short_name', 'display_name')

class EventAdmin(admin.ModelAdmin):
	list_display = ('id', 'code', 'short_name', 'display_name')

class AspectGroupAdmin(admin.ModelAdmin):
	list_display = ('id', 'caption', 'event',)
	list_filter = ('event',)

class AspectAdmin(admin.ModelAdmin):
	list_display = ('id', 'caption', 'group', 'event')
	list_filter = ('event',)

class TournamentRoundAdmin(admin.ModelAdmin):
	list_display = ('id', 'short_name', 'display_name', 'line')
	list_filter = ('event',)

class EventPlaceAdmin(admin.ModelAdmin):
	list_display = ('id', 'event', 'province', 'city', 'is_registration_open')
	list_filter = ('event',)

class GeocodeAdmin(admin.ModelAdmin):
	list_display = ('id', 'kelurahan', 'kecamatan', 'provinsi', 'kodepos')
	search_fields = ('kodepos', 'kelurahan__istartswith', 'kecamatan__istartswith',
														'kabupaten__istartswith')

class EventFAQAdmin(admin.ModelAdmin):
	list_display = ('id', 'event', 'sequence', 'question', 'created_at', 'created_by')


class SponsorAdmin(admin.ModelAdmin):
	list_display = ('id', 'short_name', 'display_name', 'event', 'created_at', 'created_by')


admin.site.register(Category,CategoryAdmin)
admin.site.register(LegalDocument, LegalDocumentAdmin)
admin.site.register(Entity, EntityAdmin)
admin.site.register(BusinessScope, BusinessScopeAdmin)
admin.site.register(BusinessSector, BusinessSectorAdmin)
admin.site.register(Event, EventAdmin)
admin.site.register(EventPlace, EventPlaceAdmin)
admin.site.register(AspectGroup, AspectGroupAdmin)
admin.site.register(Aspect, AspectAdmin)
admin.site.register(TournamentRound, TournamentRoundAdmin)
admin.site.register(Geocode, GeocodeAdmin)
admin.site.register(EventFAQ, EventFAQAdmin)
admin.site.register(Quote)
admin.site.register(Sponsor, SponsorAdmin)