from django.db import models
from django.utils.text import slugify
from django.contrib.auth.models import User
from core.dbs.base.models import BaseModel
from core.libs import storage
from core.libs.constants import EVENT_TYPE_CHOICES


class Role(models.Model):
	display_name = models.CharField(max_length=20)
	short_name = models.CharField(max_length=20, blank=True, null=True)

	def __str__(self):
		return self.short_name

	def save(self, *args, **kwargs):
		if self.display_name:
			self.short_name = slugify(self.display_name)
		super(Role, self).save(*args, **kwargs)


class Category(BaseModel):
	"""
	Company Business Sectors:
	Agtech
	Artificial intelligence
	E-Commerce
	EdTech
	FinTech
	Media
	IoT/Hardware
	Logistic
	MedTech
	On Demand
	SAAS
	Travel and Hospitality
	Other
	"""
	display_name = models.CharField(max_length=40)
	short_name = models.CharField(max_length=40, blank=True, null=True)

	def __str__(self):
		return self.short_name

	def save(self, *args, **kwargs):
		if self.display_name:
			self.short_name = slugify(self.display_name)
		super(Category, self).save(*args, **kwargs)


class LegalDocument(BaseModel):
	"""
	akta, siup dll
	"""
	display_name = models.CharField(max_length=20)
	short_name = models.CharField(max_length=20, blank=True, null=True)
	description = models.TextField(blank=True, null=True)

	def __str__(self):
		return self.short_name

	def save(self, *args, **kwargs):
		if self.display_name:
			self.short_name = slugify(self.display_name)
		super(LegalDocument, self).save(*args, **kwargs)


class Entity(BaseModel):
	""" 
	Badan usaha,
	"""
	display_name = models.CharField(max_length=20)
	short_name = models.CharField(max_length=20, blank=True, null=True)

	def __str__(self):
		return self.short_name

	def save(self, *args, **kwargs):
		if self.display_name:
			self.short_name = slugify(self.display_name)
		super(Entity, self).save(*args, **kwargs)


class BusinessScope(BaseModel):
	display_name = models.CharField(max_length=100)
	short_name = models.CharField(max_length=100, blank=True, null=True)

	def __str__(self):
		return self.display_name

	def save(self, *args, **kwargs):
		if self.display_name:
			self.short_name = slugify(self.display_name)
		super().save(*args, **kwargs)


class BusinessSector(BaseModel):
	display_name = models.CharField(max_length=100)
	short_name = models.CharField(max_length=100, blank=True, null=True)

	def __str__(self):
		return self.display_name

	def save(self, *args, **kwargs):
		if self.display_name:
			self.short_name = slugify(self.display_name)
		super().save(*args, **kwargs)



class Partner(BaseModel):
	display_name = models.CharField(max_length=64)
	short_name = models.CharField(max_length=64, blank=True, null=True)
	logo = models.FileField(storage=storage.STORAGE_COMPANY, 
											blank=True, null=True)

	def __str__(self):
		return self.short_name	


class Event(BaseModel):
	code = models.CharField(max_length=64, blank=True, null=True)
	display_name = models.CharField(max_length=64)
	short_name = models.CharField(max_length=64, blank=True, null=True)
	url = models.URLField(max_length=512, blank=True, null=True)
	type = models.CharField(max_length=15, choices=EVENT_TYPE_CHOICES)
	start_date = models.DateField()
	end_date = models.DateField()
	main_event_place = models.TextField(blank=True, null=True)
	description = models.TextField(blank=True, null=True)
	banner = models.FileField(storage=storage.STORAGE_EVENT, 
											blank=True, null=True)
	organized_by = models.ForeignKey(Partner, on_delete=models.CASCADE, 
											blank=True, null=True,
											related_name='master_event_organized_by')
	sponsors = models.ManyToManyField(Partner, blank=True,
											related_name='master_event_sponsor')

	# setting menu
	can_submit_idea = models.BooleanField(default=True)
	is_registration_open = models.BooleanField(default=False)
		
	# 	assessment
	need_assessment = models.BooleanField(default=True)
	question_count = models.IntegerField(default=5)
	passed_score = models.IntegerField(default=3)
	try_again_hours = models.IntegerField(default=3*24)

	class Meta:
		ordering = ['-created_at',]

	def __str__(self,):
		return self.short_name

	def save(self, *args, **kwargs):
		if self.display_name:
			self.short_name = slugify(self.display_name)
		super(Event, self).save(*args, **kwargs)

	class Meta:
		permissions = [
			("ds_event", "Can access event management Tab (DS)"),
			("ds_can_view_event", "Can view event (DS)"),
			("ds_can_add_event", "Can add event (DS)"),
			("ds_can_edit_event", "Can edit event (DS)"),
			("ds_can_delete_event", "Can delete event (DS)"),
		]


class EventPlace(BaseModel):
	event = models.ForeignKey(Event, on_delete=models.CASCADE)
	city = models.CharField(max_length=64)
	province = models.CharField(max_length=64)
	building_name = models.CharField(max_length=100, blank=True, null=True)
	address = models.TextField(blank=True)
	latitude = models.FloatField(default=0)
	longitude = models.FloatField(default=0)
	is_registration_open = models.BooleanField(default=False)

	def __str__(self):
		return "{}:{}".format(self.province, self.city)

	class Meta:
		unique_together = ('event', 'city', 'province')


class EventFAQ(BaseModel):
	event = models.ForeignKey(Event, on_delete=models.CASCADE)
	sequence = models.SmallIntegerField(default=1)
	question = models.CharField(max_length=256)
	answer = models.TextField()

	def __str__(self):
		return "[{}] {}".format(self.event.display_name, self.question)


class Sponsor(BaseModel):
	display_name = models.CharField(max_length=64)
	short_name = models.CharField(max_length=64, blank=True, null=True)
	logo = models.FileField(storage=storage.STORAGE_EVENT, 
											blank=True, null=True)
	event = models.ForeignKey(Event, on_delete=models.CASCADE)
	banner = models.FileField(storage=storage.STORAGE_EVENT, 
											blank=True, null=True)
	url = models.URLField(max_length=512, blank=True, null=True)


class AspectGroup(BaseModel):
	"""
	--deprecation--
	"""
	event = models.ForeignKey(Event, on_delete=models.CASCADE)
	caption = models.CharField(max_length=128)

	def __str__(self,):
		return self.caption


class Aspect(BaseModel):
	"""
	--deprecation--
	"""
	event = models.ForeignKey(Event, on_delete=models.CASCADE)
	group = models.ForeignKey(AspectGroup, on_delete=models.CASCADE)
	caption = models.CharField(max_length=128)
	description = models.TextField(blank=True)

	def __str__(self,):
		return self.caption


class TournamentRound(BaseModel):
	"""
	will be hackathon or classroom on incubation
	"""
	event = models.ForeignKey(Event, on_delete=models.CASCADE)
	display_name = models.CharField(max_length=64)
	short_name = models.CharField(max_length=64, blank=True, null=True)
	line = models.CharField(max_length=5, blank=True, null=True)
	judges = models.ManyToManyField(User, blank=True)
	
	def __str__(self,):
		return self.short_name

	def save(self, *args, **kwargs):
		if self.display_name:
			self.short_name = slugify(self.display_name)
		super(TournamentRound, self).save(*args, **kwargs)


class Geocode(models.Model):
	kelurahan = models.CharField(max_length=100)
	kecamatan = models.CharField(max_length=100)
	kabupaten = models.CharField(max_length=100)
	provinsi = models.CharField(max_length=100)
	kodepos = models.CharField(max_length=5)

	def __str__(self,):
		return self.kodepos


class Quote(models.Model):
	quote = models.TextField()
	creator = models.CharField(max_length=256)
	photo = models.ImageField(max_length=300, blank=True, null=True,
										storage=storage.STORAGE_MASTER) 

	def __str__(self):
		return self.quote