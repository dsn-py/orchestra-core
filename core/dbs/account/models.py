import uuid
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from knox.settings import CONSTANTS
from knox.models import AuthToken
from core.dbs.master.models import Event
from core.dbs.company.models import (Company)
from core.dbs.base.models import BaseAddressModel, BaseModel
from core.libs import storage
from core.libs.constants.profile import (PROFILE_GENDER_CHOICES, PROFILE_DEGREE_CHOICES)


def save_profile(sender, instance, **kwargs):
    if not Profile.objects.filter(user=instance).exists():
        Profile.objects.create(user=instance, created_by=instance)
post_save.connect(save_profile, sender=User)


class CompanyUser(BaseModel):
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    profile = models.ForeignKey('Profile', related_name='user_companies', on_delete=models.CASCADE)
    responsibility = models.TextField(blank=True, null=True)
    join_date = models.DateField(blank=True, null=True)
    leave_date = models.DateField(blank=True, null=True)
    division = models.CharField(max_length=128, blank=True, null=True)
    title = models.CharField(max_length=128)

    def __str__(self):
        return "{} | {}".format(self.company.display_name, self.profile)


class Profile(BaseAddressModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE,
                                        related_name='profile',)
    full_name = models.CharField(max_length=255, blank=True, null=True)
    bio = models.TextField(max_length=500, help_text="500 characters maximum", 
                                        blank=True, default="") 
    photo = models.ImageField(max_length=300, blank=True, null=True,
                                        storage=storage.STORAGE_USER_PHOTO) 
    job_title = models.CharField(max_length=225, blank=True, null=True)
    current_job_company = models.CharField(max_length=225, blank=True, null=True)
    current_job_url = models.URLField(max_length=300, blank=True, null=True)
    phone_number = models.CharField(max_length=20, blank=True, null=True)
    current_event = models.ForeignKey(Event, on_delete=models.CASCADE, 
                                        blank=True, null=True)
    current_company = models.ForeignKey(Company, on_delete=models.CASCADE, 
                                        blank=True, null=True)
    companies = models.ManyToManyField(Company, through='CompanyUser', related_name="companies", blank=True)
    allowed_events = models.ManyToManyField(Event, blank=True, 
                                                    related_name="profile_events")
    is_first_time = models.BooleanField(default=True)
    registration_device = models.CharField(max_length=256, blank=True, null=True)
    registration_referral = models.CharField(max_length=30, blank=True, null=True)
    registration_hash = models.CharField(max_length=256, blank=True, null=True)
    gender = models.CharField(max_length=30, default="notset", choices=PROFILE_GENDER_CHOICES)
    degree = models.CharField(max_length=50, default="notset", choices=PROFILE_DEGREE_CHOICES)
    major = models.CharField(max_length=150, blank=True, null=True)
    university = models.CharField(max_length=225, blank=True, null=True)
    birthday = models.DateField(blank=True, null=True)
    skill = models.TextField(max_length=500, blank=True, null=True)

    def __str__(self,):
        return self.user.username

    def save(self, *args, **kwargs):
        if not self.registration_hash:
            self.registration_hash = uuid.uuid4().hex
        super(Profile, self).save(*args, **kwargs)


    def set_request(self, request):
        self.request = request

    @property
    def event(self):
        """
        Current event
        """
        # regular event
        event = self.current_event

        if hasattr(self, 'request'):
            token = self.request.META.get('HTTP_AUTHORIZATION')
            if token:
                token = token.replace('token ', '')
            if token is not None and len(token) > 40: # drf token is 40 digits
                authtoken = AuthToken.objects.filter(
                    token_key = token[:CONSTANTS.TOKEN_KEY_LENGTH],
                    user = self.user).first()
                event = authtoken.auth_state_token.current_event
        return event

    @property
    def attendance(self):
        return self.user.attendee_user.filter(event=self.event).first()
    
    @property
    def role(self):
        attendance = self.attendance
        if attendance:
            return attendance.type
        return None

    @property
    def member(self):
        """
        Current member group in current event
        """
        if self.event:
            return self.user.participant_groupmembers_member.filter(
                                        group__event=self.current_event).first()
        return None
    
    @property
    def group(self):
        """
        Current group in current event
        """
        if self.member:
            return self.member.group
        return None

    @property
    def team(self):
        if self.event:
            member_of = self.user.participant_teammembers_member.filter(
                                        team__event=self.event).first()
            if member_of:
                return member_of.team
        return None

    @property
    def is_mentor(self):
        return self.member.is_mentor if self.member else False

    @property
    def is_supermentor(self):
        return self.member.is_supermentor if self.member else False
    
    @property
    def attendance_type(self):
        return self.attendance.type if self.attendance else None

    @property
    def is_leader(self):

        if self.event:
            team = self.user.participant_teammembers_member.filter(
                                        team__event=self.event).first()

            if team : 
                return team.is_leader

        return False
    
    

    

