from django.contrib import admin
from .models import Profile, CompanyUser
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin as _BaseUserAdmin
from rest_framework.authtoken.models import Token


class ProfileAdmin(admin.ModelAdmin):
	list_display = ('user', 'current_event', 'city', 'current_company')
	search_fields = ('user__username',)
	filter_horizontal = ('allowed_events',)
	list_filter = ('current_event',)

class CompanyUserAdmin(admin.ModelAdmin):
	list_display = ('profile', 'company', 'title')

admin.site.register(Profile, ProfileAdmin)
admin.site.register(CompanyUser, CompanyUserAdmin)


class UserAdmin(_BaseUserAdmin):
    list_display = _BaseUserAdmin.list_display + ("token",)
    
    def token(self, obj):
    	data = Token.objects.filter(user=obj).first()
    	if data:
    		return data.key
    	return None
    

admin.site.unregister(User)
admin.site.register(User, UserAdmin)