from django.contrib import admin
from .models import (Roadmap, Speaker, Attendee, Agenda)


class RoadmapAdmin(admin.ModelAdmin):
	list_display = ('id', 'display_name', 'event', 'address', 'city', 'start', 'end', 'type')
	list_filter = ('event',)

class SpeakerAdmin(admin.ModelAdmin):
	list_display = ('id', 'name', 'company', 'roadmap')

class AttendeeAdmin(admin.ModelAdmin):
	list_display = ('id', 'roadmap', 'user', 'code')

class AgendaAdmin(admin.ModelAdmin):
	list_display = ('id', 'roadmap', 'start', 'end', 'title')


admin.site.register(Roadmap, RoadmapAdmin)
admin.site.register(Speaker, SpeakerAdmin)
admin.site.register(Attendee, AttendeeAdmin)
admin.site.register(Agenda, AgendaAdmin)
