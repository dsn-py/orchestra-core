from datetime import datetime
from django.db import models
from django.conf import settings
from django.utils.text import slugify
from django.contrib.auth.models import User

from core.dbs.base.models import BaseModel
from core.dbs.master.models import Event
from core.libs.moment import to_timestamp 
from core.libs.constants.events import ROADMAP_TYPE_CHOICES

class Roadmap(BaseModel):
	event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name='roadmap')
	# address
	address = models.CharField(max_length=225, blank=True, null=True)
	city = models.CharField(max_length=100, blank=True, null=True)
	province = models.CharField(max_length=100, blank=True, null=True)
	country = models.CharField(max_length=3, default="IDN")
	postcode = models.CharField(max_length=10, blank=True, null=True)
	# time
	start = models.DateTimeField(blank=True, null=True)
	end = models.DateTimeField(blank=True, null=True)

	display_name = models.CharField(max_length=225)
	short_name = models.CharField(max_length=225, blank=True, null=True)
	topic = models.CharField(max_length=255, blank=True, null=True)
	description = models.TextField(blank=True, null=True)
	is_registration_open = models.BooleanField(default=False)
	type = models.CharField(default='notset', choices=ROADMAP_TYPE_CHOICES, max_length=50)

	def __str__(self):
		return self.display_name

	def save(self, *args, **kwargs):
		if self.display_name:
			self.short_name = slugify(self.display_name)
		super().save(*args, **kwargs)


class Agenda(BaseModel):
	roadmap = models.ForeignKey(Roadmap, on_delete=models.CASCADE, related_name='agenda')
	start = models.TimeField()
	end = models.TimeField()
	title = models.CharField(max_length=250)

	def __str__(self):
		return self.title


class Speaker(BaseModel):
	roadmap = models.ForeignKey(Roadmap, on_delete=models.CASCADE, related_name='speakers')
	name = models.CharField(max_length=225)
	position = models.CharField(max_length=225, blank=True, null=True)
	company = models.CharField(max_length=255, blank=True, null=True)

	def __str__(self):
		return self.name

	def save(self, *args, **kwargs):
		super(Speaker, self).save(*args, **kwargs)


class Attendee(BaseModel):
	roadmap = models.ForeignKey(Roadmap, on_delete=models.CASCADE)
	user = models.ForeignKey(User, on_delete=models.CASCADE, 
										related_name="roadshow_attendee_user")
	code = models.CharField(max_length=64, blank=True, null=True)
	rsvp = models.BooleanField(default=False)

	def save(self, *args, **kwargs):
		self.code = to_timestamp(datetime.now(), epoch=getattr(settings, 'EPOCH', None))
		super().save(*args, **kwargs)	

	def __str__(self,):
		return self.user.username