from django.contrib import admin
from .models import Configuration

class ConfigAdmin(admin.ModelAdmin):
	list_display = ('ios_version', 'android_version', 'event')

admin.site.register(Configuration, ConfigAdmin)