from uuid import uuid4
from django.db import models
from django.utils.text import slugify
from core.dbs.master.models import Event
from core.dbs.base.models import BaseModel


class Configuration(BaseModel):
	ios_version = models.CharField(max_length=10,blank=True, null=True)
	android_version = models.CharField(max_length=10,blank=True, null=True)
	playstore_url = models.URLField(blank=True, null=True)
	applestore_url = models.URLField(blank=True, null=True)
	display_name = models.CharField(max_length=255)
	short_name = models.CharField(max_length=255,  blank=True, null=True)
	application_key = models.CharField(max_length=64, blank=True, null=True)
	force_event = models.BooleanField(default=False)
	event = models.ForeignKey(Event, blank=True, null=True, on_delete=models.CASCADE)

	def __str__(self):
		return "{}|{}".format(self.short_name, self.application_key)

	def save(self, *args, **kwargs):
		if self.display_name and not self.short_name:
			self.short_name = slugify(self.display_name)
		if not self.application_key:
			self.application_key = uuid4().hex
		super().save(*args, **kwargs)
