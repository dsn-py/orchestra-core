from django.contrib import admin
from .models import (UserActivity, Attendance, ForumActivity,
				MaterialActivity, TasklistActivity, ProgressActivity)


@admin.register(UserActivity)
class UserActivityAdmin(admin.ModelAdmin):
	list_display = ('created_date', 'event', 'place', 'users', 'sessions',)

@admin.register(Attendance)
class AttendanceAdmin(admin.ModelAdmin):
	list_display = ('created_date', 'event', 'place', 'total_attendance', 
												'total_user', 'percentage')

@admin.register(ForumActivity)
class ForumActivityAdmin(admin.ModelAdmin):
	list_display = ('created_at', 'event', 'threads', 'mentors', 'users')

@admin.register(MaterialActivity)
class MaterialActivityAdmin(admin.ModelAdmin):
	list_display = ('created_at', 'event', 'materials', 'downloaded', 'videos', 
																	'documents')

@admin.register(TasklistActivity)
class TasklistActivityAdmin(admin.ModelAdmin):
	list_display = ('created_date', 'event', 'place', 'weekday', 'submits')

@admin.register(ProgressActivity)
class ProgressActivityAdmin(admin.ModelAdmin):
	list_display = ('created_date', 'event', 'place', 'weekday', 'submits', 
																'discussions')
