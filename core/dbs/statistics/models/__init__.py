from django.db import models
from django.utils import timezone
from core.libs.moment import week_of_month
from core.dbs.master.models import Event, EventPlace


class UserActivity(models.Model):
	event = models.ForeignKey(Event, on_delete=models.CASCADE)
	place = models.ForeignKey(EventPlace, on_delete=models.CASCADE, blank=True, null=True)
	created_date = models.DateField()
	weekday = models.CharField(max_length=10, blank=True, null=True)
	week_num = models.SmallIntegerField(blank=True, null=True)
	users = models.IntegerField()
	sessions = models.IntegerField()

	def __str__(self):
		return "{} [{}] :: users {}, sessions {}".format(self.event.display_name,
									self.created_date, self.users, self.sessions)

	def save(self, *args, **kwargs):
		self.weekday = self.created_date.strftime("%A")
		self.week_num = week_of_month(self.created_date)
		super().save(*args, **kwargs)


	class Meta:

		permissions = [
            ("ds_statistics", "Can view statistics (DS)"),
        ]


class Attendance(models.Model):
	event = models.ForeignKey(Event, on_delete=models.CASCADE)
	place = models.ForeignKey(EventPlace, on_delete=models.CASCADE, blank=True, null=True)
	created_date = models.DateField()
	weekday = models.CharField(max_length=10, blank=True, null=True)
	week_num = models.SmallIntegerField(blank=True, null=True)
	total_attendance = models.IntegerField()
	total_user = models.IntegerField()
	percentage = models.FloatField(blank=True, null=True)

	def __str__(self):
		return "{} [{}] ::{} [{}]".format(self.event.display_name,
								self.created_date, self.total_attendance, self.percentage)

	def save(self, *args, **kwargs):
		self.weekday = self.created_date.strftime("%A")
		self.week_num = week_of_month(self.created_date)
		if self.total_user == 0:
			self.percentage = 0
		else:
			self.percentage = self.total_attendance / float(self.total_user)
		super().save(*args, **kwargs)


class ForumActivity(models.Model):
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	event = models.ForeignKey(Event, on_delete=models.CASCADE)
	threads = models.IntegerField()
	mentors = models.IntegerField()
	users = models.IntegerField()

	def __str__(self):
		return self.event.display_name


class MaterialActivity(models.Model):
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	event = models.ForeignKey(Event, on_delete=models.CASCADE)
	materials = models.IntegerField()
	downloaded = models.IntegerField(default=0)
	videos = models.IntegerField(default=0)
	articles = models.IntegerField(default=0)
	documents = models.IntegerField(default=0)

	def __str__(self):
		return self.event.display_name

class TasklistActivity(models.Model):
	event = models.ForeignKey(Event, on_delete=models.CASCADE)
	place = models.ForeignKey(EventPlace, on_delete=models.CASCADE, blank=True, null=True)
	created_date = models.DateField()
	weekday = models.CharField(max_length=10, blank=True, null=True)
	week_num = models.SmallIntegerField(blank=True, null=True)
	submits = models.IntegerField(default=0)
	posted = models.IntegerField(default=0)
	checked = models.IntegerField(default=0)

	def __str__(self):
		return "{} [{}] :: {}".format(self.event.display_name,
								self.created_date, self.submits)

	def save(self, *args, **kwargs):
		self.weekday = self.created_date.strftime("%A")
		self.week_num = week_of_month(self.created_date)
		super().save(*args, **kwargs)



class ProgressActivity(models.Model):
	event = models.ForeignKey(Event, on_delete=models.CASCADE)
	place = models.ForeignKey(EventPlace, on_delete=models.CASCADE, blank=True, null=True)
	created_date = models.DateField()
	weekday = models.CharField(max_length=10, blank=True, null=True)
	week_num = models.SmallIntegerField(blank=True, null=True)
	submits = models.IntegerField(default=0)
	checked = models.IntegerField(default=0)
	discussions = models.IntegerField(default=0)

	def __str__(self):
		return "{} [{}] :: {}".format(self.event.display_name,
								self.created_date, self.submits)

	def save(self, *args, **kwargs):
		self.weekday = self.created_date.strftime("%A")
		self.week_num = week_of_month(self.created_date)
		super().save(*args, **kwargs)


		