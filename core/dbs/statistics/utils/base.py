from datetime import timedelta
from core.dbs.statistics.models import (UserActivity, Attendance,
        ForumActivity, MaterialActivity, TasklistActivity, ProgressActivity)
from core.dbs.participant.models import Attendee
from core.dbs.materials.models import ClassMaterial
from core.dbs.master.models import EventPlace
from core.dbs.progress.models import ProgressReport, ProgressDiscussion
from core.dbs.activity.models import ActivitySignal
from core.dbs.discussion.models import Forum, Comments
from core.libs.moment import get_last_monday, get_next_monday


def recap_user_activity_event(event, now):
    places = EventPlace.objects.filter(event=event)
    for place in places:
        signal = ActivitySignal.objects.filter(
            user__attendee_user__place=place,
            created_at__date=now.date(),
        )
        UserActivity.objects.update_or_create(
            event=event,
            place=place,
            created_date=now.date(),
            defaults={
                'users':signal.order_by('user').distinct('user').count(),
                'sessions':signal.count()
            }
        )


def recap_attendance_event(event, now):
    back = get_last_monday(now)
    forward = get_next_monday(now)

    places = EventPlace.objects.filter(event=event)
    for place in places:
        attendees = Attendee.objects.filter(
            event=event,
            place=place,
            type='participant'
        ).count()

        signal = ActivitySignal.objects.filter(
            user__attendee_user__place=place,
            created_at__range=[back, forward],
            type="attend",
        ).order_by('user').distinct('user')

        Attendance.objects.update_or_create(
            event=event,
            place=place,
            created_date=back,
            defaults={
                'total_user':attendees,
                'total_attendance':signal.count(),
            }
        )


def recap_forum_event(event, now):
    forum = Forum.objects.filter(event=event)
    comments = Comments.objects.filter(forum__event=event)
    
    # by mentor
    mentor_threads = forum.filter(
        user__attendee_user__type="mentor",
    ).order_by('user').distinct('user').values_list('user', flat=True)
    mentor_comments = comments.filter(
        user__attendee_user__type="mentor",
    ).order_by('user').distinct('user').values_list('user', flat=True)
    mentors = set(list(mentor_threads) + list(mentor_comments))

    # by participant
    user_threads = forum.filter(
        user__attendee_user__type="participant",
    ).order_by('user').distinct('user').values_list('user', flat=True)
    user_comments = comments.filter(
        user__attendee_user__type="participant",
    ).order_by('user').distinct('user').values_list('user', flat=True)
    users = set(list(user_comments) + list(user_threads))

    ForumActivity.objects.update_or_create(
        event=event,
        defaults={
            'threads': forum.count(),
            'users': len(users),
            'mentors': len(mentors)
        }
    )


def recap_materials_event(event, now):
    materials = ClassMaterial.objects.filter(event=event)
    videos = materials.filter(type='stream').values_list('id', flat=True)
    articles = materials.filter(type='link').values_list('id', flat=True)
    documents = materials.filter(type='document').values_list('id', flat=True)
    signal = ActivitySignal.objects.filter(
            event=event,
            type="material",
        )
    signal_videos = signal.filter(object_id__in=videos)
    signal_document = signal.filter(object_id__in=documents)
    signal_articles = signal.filter(object_id__in=articles)

    MaterialActivity.objects.update_or_create(
        event=event,
        defaults={
            'materials' : materials.count(),
            'downloaded' : signal.count(),
            'videos' : len(signal_videos),
            'articles': len(signal_articles),
            'documents' : len(signal_document),
        }
    )


def recap_tasklist_event(event, now):
    back = get_last_monday(now)
    forward = get_next_monday(now)

    places = EventPlace.objects.filter(event=event)
    for place in places:
        datenow = back
        while datenow < forward:
            signal = ActivitySignal.objects.filter(
                user__attendee_user__place=place,
                created_at__date = datenow,
                type="submit-task",
            ).order_by('user').distinct('user')

            signal2 = ActivitySignal.objects.filter(
                user__attendee_user__place=place,
                created_at__date = datenow,
                type="create-task-mentor",
            ).order_by('user').distinct('user')

            signal3 = ActivitySignal.objects.filter(
                user__attendee_user__place=place,
                created_at__date = datenow,
                type="check-task-mentor",
            ).order_by('user').distinct('user')

            TasklistActivity.objects.update_or_create(
                event=event,
                place=place,
                created_date=datenow,
                defaults={
                    'submits': signal.count(),
                    'posted': signal2.count(),
                    'checked': signal3.count(),
                }
            )
            datenow = datenow + timedelta(days=1)


def recap_progress_event(event, now):
    back = get_last_monday(now)
    forward = get_next_monday(now)

    places = EventPlace.objects.filter(event=event)
    for place in places:
        datenow = back
        while datenow < forward:
            progress = ActivitySignal.objects.filter(
                user__attendee_user__place=place,
                created_at__date = datenow,
                type="submit-progress",
            ).order_by('user').distinct('user')

            checked = ActivitySignal.objects.filter(
                user__attendee_user__place=place,
                created_at__date = datenow,
                type="check-progress-mentor",
            ).order_by('user').distinct('user')

            discussion = ActivitySignal.objects.filter(
                user__attendee_user__place=place,
                created_at__date = datenow,
                type__in=['discuss', 'discuss-mentor'],
            ).order_by('user')

            # progress = ProgressReport.objects.filter(
            #     user__attendee_user__place=place,
            #     created_at__date = datenow,
            # ).order_by('user').distinct('user')

            # discussion = ProgressDiscussion.objects.filter(
            #     user__attendee_user__place=place,
            #     created_at__date = datenow,
            # ).order_by('user')

            ProgressActivity.objects.update_or_create(
                event=event,
                place=place,
                created_date=datenow,
                defaults={
                    'submits':progress.count(),
                    'checked': checked.count(),
                    'discussions': discussion.count()
                }
            )
            datenow = datenow + timedelta(days=1)

