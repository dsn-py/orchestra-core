from django.utils import timezone
from core.dbs.master.models import Event
from .base import *


def recap_user_activity():
    now = timezone.now()
    events = Event.objects.filter(
        start_date__lte=now,
        end_date__gte=now,
        type='incubation',
    ).values_list('id', flat=True)

    for event in events:
        recap_user_activity_event(event, now)


def recap_attendance():
    now = timezone.now()
    events = Event.objects.filter(
        start_date__lte=now,
        end_date__gte=now,
        type='incubation',
    ).values_list('id', flat=True)

    for event in events:
        recap_attendance_event(event, now)


def recap_forum():
    now = timezone.now()
    events = Event.objects.filter(
        start_date__lte=now,
        end_date__gte=now,
        type='incubation',
    ).values_list('id', flat=True)

    for event in events:
        recap_forum_event(event, now)


def recap_materials():
    now = timezone.now()
    events = Event.objects.filter(
        start_date__lte=now,
        end_date__gte=now,
        type='incubation',
    ).values_list('id', flat=True)

    for event in events:
        recap_materials_event(event, now)


def recap_tasklist():
    now = timezone.now()
    events = Event.objects.filter(
        start_date__lte=now,
        end_date__gte=now,
        type='incubation',
    ).values_list('id', flat=True)

    for event in events:
        recap_tasklist_event(event, now)


def recap_progress():
    now = timezone.now()
    events = Event.objects.filter(
        start_date__lte=now,
        end_date__gte=now,
        type='incubation',
    ).values_list('id', flat=True)

    for event in events:
        recap_progress_event(event, now)