from django.contrib import admin
from .models import Activity, ActivityLog, ActivitySignal


@admin.register(ActivityLog)
class ActivityLogAdmin(admin.ModelAdmin):
	list_display = ('id', 'latest_activity', 'user', 'event')
	list_search = ('user__username__istartswith', )
	extras = 0


@admin.register(Activity)
class ForumAdmin(admin.ModelAdmin):
	list_display = ('id', 'user', 'event', 'model', 'object_id')
	list_search = ('user__username__istartswith', )


@admin.register(ActivitySignal)
class SignalAdmin(admin.ModelAdmin):
	list_display = ('id', 'user', 'created_at', 'type')
	list_search = ('user__username', 'type')
	list_filter = ('type', 'event',)