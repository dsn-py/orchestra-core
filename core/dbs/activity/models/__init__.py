from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from core.dbs.base.models import BaseModel
from core.dbs.master.models import Event
from core.dbs.materials.models import ClassMaterial


class Activity(BaseModel):
    """
    user based logging, specially for mentor 
    """
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE, 
                                        related_name="activity_activity_user")
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    model = models.CharField(max_length=50, blank=True, null=True)
    
    def __str__(self):
        return self.user.username


class ActivityLog(models.Model):
    """
    store latest activity ID.
    """
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE, 
                                        related_name="activity_log_user")
    latest_activity = models.ForeignKey(Activity, on_delete=models.CASCADE, 
                                        blank=True, null=True)

    def __str__(self):
        return "{} ({})".format(self.user.username, self.latest_activity_id)



class ActivitySignal(BaseModel):
    ACTIVITY_SIGNAL_CHOICES = (
        ('resume', 'Resume Back To Apps'),
        ('attend', 'Attend offline event'),
        ('material', 'Open Material'),
        ('create-task-mentor', 'Create a task'),
        ('submit-task', 'Response to a Tasklist'),
        ('submit-progress', 'Submit a Progress'),
        ('check-progress-mentor', 'Mentor check progress'),
        ('check-task-mentor', 'Mentor check task'),
        ('open-topic', 'Add Forum Topic'),
        ('comment-topic', 'Comment a Topic'),
        ('discuss', 'Open Discussion with Mentor (Progress Tracking)'),
        ('discuss-mentor', 'Mentor reply discussion (Progress Tracking)')
    )
    base_event_time = models.CharField(max_length=30, blank=True, null=True)
    is_first_day = models.BooleanField(default=False)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE, 
                                        related_name="activity_activitysignal_user")
    type = models.CharField(max_length=30, choices=ACTIVITY_SIGNAL_CHOICES)
    extras = models.TextField(blank=True, null=True)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, blank=True, null=True)

    object_id = models.PositiveIntegerField(blank=True, null=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    extra_param1 = models.CharField(max_length=30, blank=True, null=True)

    def __str__(self):
        return "{}-{}".format(self.user.username, self.type)

    class Meta:
        ordering = ('-created_at',)
 
    def save(self, *args, **kwargs):
        now = timezone.now()
        if self.type == 'resume':
            search = ActivitySignal.objects.filter(
                user=self.user,
                type='resume',
                created_at__date=now.date()
            )
            if not search:
                self.extras = "firstday"
                self.is_first_day = True

        elif self.type == 'material':
            material = ClassMaterial.objects.get(id=int(self.extras))
            self.content_object = material

        if not self.base_event_time:
            weekday = str(now.weekday())
            base_hour = str(now.hour)
            date = str(now.date())
            self.base_event_time = "{}|{}|{}".format(date, weekday, base_hour)
        super().save(*args, **kwargs)



def register_signal(event, user, signal_type, extras=None, content_object=None, 
                                            extra_param1=None, throttle=3600):
    latest = ActivitySignal.objects.filter(
        user=user,
        type=signal_type,
    ).first()

    if latest:
        ts = (timezone.now() - latest.created_at).seconds
    else:
        ts = throttle + 1
    if (ts < throttle):
        return

    signal = ActivitySignal.objects.create(
        created_by=user,
        user=user,
        type=signal_type,
        extras=extras,
        event=event,
    )
    if content_object:
        signal.content_object = content_object
        signal.save()
