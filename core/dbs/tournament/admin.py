from django.contrib import admin
from .models import (Aspect, Team, Phase, ScoreDetail, Category, ScoreRecap, Idea, Competition,
													WinningSchema, AspectInPhase)
from core.libs.django.admin import RawFieldMixins

class IdeaInline(admin.TabularInline):
	model = Idea
	extra = 0

class CompetitionInline(admin.TabularInline):
	model = Competition
	extra = 1

class AspectInPhaseInline(admin.TabularInline, RawFieldMixins):
	model = AspectInPhase
	extra = 0
	list_select_related = True
	raw_id_fields = ('aspect',)


@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
	list_display = ('display_name', 'event', 'is_assessment_passed', 'assessment_score', )
	inlines = [IdeaInline,]
	list_filter = ('event', 'is_qualified', 'is_assessment_passed')
	search_fields = ("display_name",)

class AspectAdmin(admin.ModelAdmin):
    list_display = ('id', 'caption', 'event')
    list_filter = ("event",)


class PhaseAdmin(admin.ModelAdmin, RawFieldMixins):
    list_display = ('name', 'event', 'line', 'is_active', )
    list_filter = ("event",)
    inlines = (CompetitionInline, AspectInPhaseInline)
    raw_id_fields = ('created_by', 'updated_by', 'judges', )
    list_select_related = True


class ScoreDetailAdmin(admin.ModelAdmin):
    list_display = ('judge', 'competition', 'aspect', 'score', 'total')

class CategoryAdmin(admin.ModelAdmin):
    list_display = ('display_name', 'short_name', 'event')

class ScoreAdmin(admin.ModelAdmin):
    list_display = ('team', 'total', 'phase')

class IdeaAdmin(admin.ModelAdmin):
	list_display = ('team', 'title', 'category', 'qualification')
	list_filter = ("team__event",)


def reset_score(modeladmin, request, queryset):
	queryset.update(total=0, winning_position=None, set_winner_by=None)
	ScoreRecap.objects.filter(competition__in=queryset).delete()
	ScoreDetail.objects.filter(competition__in=queryset).delete()
reset_score.short_description = "Reset all score and winning position to zero"

@admin.register(Competition)
class CompetitionAdmin(admin.ModelAdmin):
	list_display = ('event', 'updated_at', 'phase', 'team', 'sequence', 'winning_position', 'is_favorite', 'total')
	list_filter = ('phase__event', 'is_favorite', )
	actions = [reset_score, ]

	def event(self, obj):
		return obj.phase.event


@admin.register(WinningSchema)
class WinningSchemaAdmin(admin.ModelAdmin):
	list_display = ('event', 'name', 'can_redundant')
	list_filter = ('event',)

admin.site.register(Aspect, AspectAdmin)
admin.site.register(Phase, PhaseAdmin)
admin.site.register(ScoreDetail, ScoreDetailAdmin)
admin.site.register(Category, CategoryAdmin) 
admin.site.register(ScoreRecap, ScoreAdmin)
admin.site.register(Idea, IdeaAdmin)