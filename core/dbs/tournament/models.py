from django.db import models
from django.utils.text import slugify
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator
from core.libs import storage
from core.libs.constants.tournament import IDEA_STATUS_CHOICES
from core.dbs.master.models import Event
from core.dbs.base.models import BaseModel
from core.dbs.participant.models import Team


class Category(BaseModel):
    event = models.ForeignKey(Event, on_delete=models.CASCADE, 
                                                blank=True, null=True)
    display_name = models.CharField(max_length=128,)
    short_name = models.CharField(max_length=128, blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.display_name
    
    def save(self, *args, **kwargs):
        self.short_name = slugify(self.display_name)
        super().save(*args, **kwargs)


class Idea(BaseModel):
    category = models.ForeignKey(Category, on_delete=models.CASCADE, 
                                                blank=True, null=True)
    team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name='tournament_idea_team')
    title = models.CharField(max_length=200, blank=True, null=True)
    short_description = models.CharField(max_length=150)
    long_description = models.TextField()
    document = models.FileField(storage=storage.STORAGE_IDEATION, 
                                            blank=True, null=True)
    url = models.URLField(blank=True, null=True, max_length=512)
    qualification = models.CharField(default='waiting', choices=IDEA_STATUS_CHOICES, 
                                                                    max_length=50)
    technology = models.CharField(max_length=255, )
    
    def __str__(self):
        return "[{} :: {}] {}".format(self.team.event.display_name, self.team.display_name, self.title)


class Aspect(BaseModel):
    sequence = models.PositiveSmallIntegerField(default=1)
    event = models.ForeignKey(Event, on_delete=models.CASCADE, 
                                related_name="tournament_aspect_event")
    caption = models.CharField(max_length=128)
    description = models.TextField(blank=True)
    weight = models.FloatField(default=0, validators=[
                               MaxValueValidator(1), MinValueValidator(0)])

    def __str__(self,):
    	return "{} :: {}".format(self.event.display_name, self.caption)

    class Meta:
        permissions = [
            ("ds_can_view_aspect", "Can view aspect (DS)"),
            ("ds_can_add_aspect", "Can add aspect (DS)"),
            ("ds_can_edit_aspect", "Can edit aspect (DS)"),
            ("ds_can_delete_aspect", "Can delete aspect (DS)"),
        ]


class Phase(BaseModel):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    name = models.CharField(max_length=128, blank=True)
    line = models.CharField(max_length=5, blank=True, null=True)
    is_active = models.BooleanField(default=False)
    judges = models.ManyToManyField(User, blank=True)
    aspects = models.ManyToManyField(Aspect, through="AspectInPhase", blank=True)
    
    def __str__(self):
        return "{} :: {} :: {}".format(self.event.display_name, self.name, 
                            self.line if self.line is not None else "")

    class Meta:
        permissions = [
            ("ds_can_view_phase", "Can view phase (DS)"),
            ("ds_can_add_phase", "Can add phase (DS)"),
            ("ds_can_edit_phase", "Can edit phase (DS)"),
            ("ds_can_delete_phase", "Can delete phase (DS)"),
            ("ds_can_assign_team_to_phase", "Can assign team to phase (DS)"),
            ("ds_can_remove_team_to_phase", "Can remove team to phase (DS)"),
        ]


class AspectInPhase(models.Model):
    aspect = models.ForeignKey(Aspect, on_delete=models.CASCADE)
    phase = models.ForeignKey(Phase, on_delete=models.CASCADE)

    class Meta:
        unique_together = ("aspect", "phase")


class WinningSchema(BaseModel):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    name = models.CharField(max_length=225)
    can_redundant = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Competition(BaseModel):
    phase = models.ForeignKey(Phase, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    sequence = models.PositiveSmallIntegerField(default=1)
    total = models.FloatField(default=0)
    winning_position = models.ForeignKey(WinningSchema, on_delete=models.SET_NULL, 
                                                        blank=True, null=True)
    set_winner_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="competition_winner_setter",
                                                        blank=True, null=True)
    is_favorite = models.BooleanField(default=False)
    set_favorite_by = models.ForeignKey(User, on_delete=models.SET_NULL, related_name="competition_favorite_setter", 
                                                        blank=True, null=True)
    def __str__(self):
        return self.team.display_name + " : " + str(self.total)

    class Meta:
        permissions = [
            ("ds_scores", "Can access tournament Tab (DS)"),
            ("ds_can_view_scores", "Can view scores (DS)"),
            ("ds_can_add_scores", "Can add scores (DS)"),
            ("ds_can_edit_scores", "Can edit scores (DS)"),
            ("ds_can_delete_scores", "Can delete scores (DS)"),
        ]


class ScoreRecap(BaseModel):
    competition = models.ForeignKey(Competition, on_delete=models.CASCADE)
    phase = models.ForeignKey(Phase, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    judge = models.ForeignKey(User, on_delete=models.CASCADE)
    total = models.FloatField(default=0)

    def __str__(self):
        return self.team.display_name + " : " + str(self.total)

    class Meta:
        ordering = ['team', 'phase', 'competition__sequence']


class ScoreDetail(BaseModel):
    competition = models.ForeignKey(Competition, on_delete=models.CASCADE, )
    judge = models.ForeignKey(User, on_delete=models.CASCADE)
    aspect = models.ForeignKey(Aspect, on_delete=models.CASCADE)
    score = models.SmallIntegerField(
        blank=True,
        null=True,
        validators=[MaxValueValidator(5), MinValueValidator(1)]
    )
    total = models.FloatField(blank=True, null=True)
    
    def __str__(self):
        return str(self.total)

    def save(self, *args, **kwargs):
        self.total = round(self.score * self.aspect.weight, 2)
        super().save(*args, **kwargs)

        competition = self.competition

        recap, created = ScoreRecap.objects.get_or_create(
            phase=competition.phase,
            team=competition.team,
            judge=self.judge,
            created_by=self.created_by,
            competition=competition
        )
        
        detail = ScoreDetail.objects.filter(
            judge=self.judge,
            competition=competition,
        ).aggregate(models.Sum('total'))

        grandtotal = ScoreDetail.objects.filter(
            competition=self.competition,
        ).aggregate(models.Sum('total'))
        
        recap.total = round(detail["total__sum"], 2)
        recap.save()

        competition.total = round(grandtotal["total__sum"], 2)
        competition.save()
