from django.contrib import admin
from .models import (Applicant, ApplicantCompany, CompanyTech)


class ApplicantAdmin(admin.ModelAdmin):
	list_display = ('id', 'first_name', 'email', 'current_job_position', 'company')

class ApplicantCompanyAdmin(admin.ModelAdmin):
	list_display = ('id', 'display_name', 'business_sector', 'status', 'curated_by')

class CompanyTechAdmin(admin.ModelAdmin):
	list_display = ('id', 'company', 'name')


admin.site.register(Applicant,ApplicantAdmin)
admin.site.register(ApplicantCompany,ApplicantCompanyAdmin)
admin.site.register(CompanyTech,CompanyTechAdmin)