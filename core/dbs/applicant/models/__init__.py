from django.db import models
from core.dbs.base.models import BaseModel
from django.contrib.auth.models import User
from core.dbs.master.models import Entity, BusinessScope, BusinessSector
from core.dbs.participant.models import Event
from django.utils.text import slugify
from core.libs.constants.profile import (PROFILE_GENDER_CHOICES, PROFILE_DEGREE_CHOICES)


class Applicant(BaseModel):
    company = models.ForeignKey("ApplicantCompany", on_delete=models.SET_NULL, blank=True, null=True, )
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=256)
    email = models.EmailField(max_length=256)
    phone_number = models.CharField(max_length=20)
    location = models.CharField(max_length=256)
    birthdate = models.DateField(blank=True, null=True)
    current_job_company = models.CharField(max_length=225, blank=True, null=True, )
    current_job_position = models.CharField(max_length=125, blank=True, null=True)
    current_job_url = models.URLField(max_length=300, blank=True, null=True)
    in_startup_position = models.CharField(max_length=125, blank=True, null=True)
    event = models.ForeignKey(Event, on_delete=models.CASCADE, )
    gender = models.CharField(max_length=30, default="notset", choices=PROFILE_GENDER_CHOICES)
    degree = models.CharField(max_length=50, default="notset", choices=PROFILE_DEGREE_CHOICES)
    major = models.CharField(max_length=150, blank=True, null=True)
    university = models.CharField(max_length=225, blank=True, null=True)
    skill = models.TextField(max_length=500, blank=True, null=True)
    is_leader = models.BooleanField(default=False, blank=True, null=True)

    def __str__(self):
        return self.first_name + " " + self.last_name

    class Meta:
        permissions = [
            ("ds_applicant", "Can access applicant Tab (DS)"),
            ("ds_can_view_applicant", "Can view applicant (DS)"),
            ("ds_can_add_applicant", "Can add applicant (DS)"),
            ("ds_can_edit_applicant", "Can edit applicant (DS)"),
            ("ds_can_delete_applicant", "Can delete applicant (DS)"),
        ]


class ApplicantCompany(BaseModel):
    STATUS_CHOICES = (
        ('idea', "Idea"),
        ('on development', "Ongoing Development"),
        ('running', "Running"),
    )
    CURATED_RESULT_CHOICES = (
        ('not-checked', "Not checked yet"),
        ('not-qualified', "Not qualified"),
        ('on-consideration', "Not really good but promising. (marking it for later use)"),
        ('qualified', "Qualified")
    )
    # event = models.ForeignKey(Event, on_delete=models.CASCADE, blank=True, null=True)
    creator = models.EmailField(blank=True, null=True)
    curated_by = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True)
    curated_at = models.DateTimeField(blank=True, null=True)
    result = models.CharField(max_length=30, choices=CURATED_RESULT_CHOICES)
    display_name = models.CharField(max_length=200, blank=True, null=True)
    short_name = models.CharField(max_length=128, blank=True, null=True)
    user_position = models.CharField(max_length=100, blank=True, null=True)
    introduction = models.TextField(blank=True)
    presentation = models.URLField(max_length=256, blank=True, null=True)
    website = models.URLField(max_length=256, )
    business_sector = models.ForeignKey(BusinessSector, on_delete=models.SET_NULL, blank=True, null=True)
    business_sector_other = models.CharField(max_length=100, blank=True, null=True)
    business_scope = models.ManyToManyField(BusinessScope,)
    business_scope_other = models.CharField(max_length=100, blank=True, null=True)
    total_founders = models.IntegerField(default=1)
    has_marketing_cofounder = models.BooleanField(default=False)
    has_creative_cofounder = models.BooleanField(default=False)
    has_tech_cofounder = models.BooleanField(default=False)
    total_employees = models.IntegerField(default=0)
    entity_type = models.ForeignKey(Entity, on_delete=models.SET_NULL, blank=True, null=True)
    last_funded = models.CharField(max_length=4, blank=True, null=True)
    round_investment = models.CharField(max_length=200, blank=True, null=True)
    status = models.CharField(max_length=30, choices=STATUS_CHOICES)
    last_month_revenue = models.BigIntegerField(default=0, blank=True, null=True)
    monthly_active_users = models.IntegerField(default=0)
    tech_stack = models.CharField(max_length=255, blank=True, null=True)

    @property
    def all_business_scope(self):
      return ', '.join([x.display_name for x in self.business_scope.all()])

    def save(self, *args, **kwargs):
        if self.display_name:
            self.short_name = slugify(self.display_name)
        super(ApplicantCompany, self).save(*args, **kwargs)

    class Meta:

        permissions = [
                ("ds_can_view_applicant_company", "Can view applicant company (DS)"),
                ("ds_can_add_applicant", "Can add applicant company (DS)"),
                ("ds_can_edit_applicant", "Can edit applicant company (DS)"),
                ("ds_can_delete_applicant", "Can delete applicant company (DS)"),
            ]


class CompanyTech(BaseModel):
    company = models.ForeignKey(ApplicantCompany, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name