import uuid
import random
from datetime import timedelta
from django.db import models
from django.utils import timezone
from django.conf import settings
from django.utils.text import slugify
from django.contrib.auth.models import User
from django.utils.crypto import get_random_string
from core.libs import storage
from core.dbs.base.models import BaseModel
from core.dbs.master.models import Event
from core.dbs.participant.models import Team
from core.dbs.master.utils.assessment import get_try_again_hours


class Account(BaseModel):
	user = models.OneToOneField(User, on_delete=models.CASCADE)

	def __str__(self):
		return self.user.username


class QuestionCategory(BaseModel):
	display_name = models.CharField(max_length=100)
	short_name = models.CharField(max_length=100, blank=True, null=True)

	def __str__(self):
		return self.display_name

	def save(self, *args, **kwargs):
		self.short_name = slugify(self.display_name)
		super().save(*args, **kwargs)

	class Meta:

		permissions = [
				("ds_can_view_category_assessment", "Can view category assessment (DS)"),
				("ds_can_add_category_assessment", "Can add category assessment (DS)"),
				("ds_can_edit_category_assessment", "Can edit category assessment (DS)"),
				("ds_can_delete_category_assessment", "Can delete category assessment (DS)"),
			]


class Project(BaseModel):
	question_category = models.ForeignKey(QuestionCategory, on_delete=models.CASCADE, blank=True, null=True)
	code = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
	secret = models.CharField(max_length=64, blank=True, null=True)
	user = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True)
	display_name = models.CharField(max_length=200)
	short_name = models.CharField(max_length=200, blank=True, null=True)
	description = models.TextField(blank=True)
	total_questions = models.IntegerField(default=10)
	passed_score = models.IntegerField(default=7)
	duration_minutes = models.IntegerField(default=60)
	try_again_hours = models.IntegerField(default=3*24)
	end_at = models.DateTimeField(blank=True, null=True)

	def __str__(self):
		return self.short_name

	def save(self, *args, **kwargs):
		self.short_name = slugify(self.display_name)
		if not self.secret:
			self.secret = get_random_string(length=64)
		super().save(*args, **kwargs)

	class Meta:
		permissions = [
				("ds_assessment", "Can access assessment tab DS"),
				("ds_can_view_project", "Can view project assessment (DS)"),
				("ds_can_add_project", "Can add project assessment (DS)"),
				("ds_can_edit_project", "Can edit project assessment (DS)"),
				("ds_can_delete_project", "Can delete project assessment (DS)"),
			]

class Rules(BaseModel):
	project = models.ForeignKey(Project, blank=True, null=True, on_delete=models.SET_NULL)
	sequence = models.SmallIntegerField(default=1) 
	description = models.TextField(blank=True)

	def __str__(self):
		return self.project.display_name


class Participant(BaseModel):
	project = models.ForeignKey(Project, on_delete=models.CASCADE)
	email = models.EmailField(max_length=300)
	name = models.CharField(max_length=300)
	session_key = models.CharField(max_length=128, blank=True, null=True)
	last_session = models.ForeignKey("AssessmentSession", blank=True, null=True, on_delete=models.SET_NULL, 
		related_name='participant_last_session')

	def __str__(self):
		return self.name

	# [POST]assessment.orchestra.co.id/key/get/ 
	# payload = {"project": <apa>, "email": <apa>}
	# response = {"email": xxx , "nama": yyy, "session_key": zzz}
	# assessment.orchestra.co.id/test/<session key>/

	def save(self, *args, **kwargs):
		if not self.session_key:
			self.session_key = get_random_string(length=128)
		super().save(*args, **kwargs)

	class Meta:
		permissions = [
				("ds_can_view_participant_assessment", "Can view participant assessment (DS)"),
				("ds_can_add_participant_assessment", "Can add participant assessment (DS)"),
				("ds_can_edit_partiicpnat_assessment", "Can edit participant assessment (DS)"),
				("ds_can_delete_participant_assessment", "Can participant assessment (DS)"),
			]
 
 
class Question(BaseModel):
	QUESTION_TYPE_CHOICES = (
		("choices", "Choices"),
		("short-answer", "Short Answer"),
	)
	# v1 ignore this for now
	event = models.ForeignKey(Event, on_delete=models.SET_NULL, 
						help_text='Use event only for special question on the event.',
						blank=True, null=True)
	# v2 ignore this for now
	project = models.ForeignKey(Project, on_delete=models.SET_NULL, blank=True, null=True)
	photo = models.ImageField(max_length=300, blank=True, null=True,
										storage=storage.STORAGE_ASSESSMENT)
	question_type = models.CharField(max_length=20, choices=QUESTION_TYPE_CHOICES, default="choices")
	question_category = models.ForeignKey(QuestionCategory, on_delete=models.CASCADE, blank=True, null=True)
	title = models.CharField(max_length=150, )
	text = models.TextField(blank=True)
	random_weight = models.FloatField(default=1.0)
	usages = models.IntegerField(default=0)
	succeeded = models.IntegerField(default=0)
	answer = models.CharField(max_length=200, blank=True, null=True)

	def __str__(self):
		return self.title

	def get_id_weight(self):
		return Question.objects.all().values_list('id', 'random_weight', flat=True)

	def formula(self):
		# just a sample usage
		import random
		random.choices(['one', 'two', 'three'], [0.2, 0.3, 0.5], k=10)

	class Meta:
		permissions = [
				("ds_assessment", "Can access assessment tab DS"),
				("ds_can_view_question", "Can view question assessment (DS)"),
				("ds_can_add_question", "Can add question assessment (DS)"),
				("ds_can_edit_question", "Can edit question assessment (DS)"),
				("ds_can_delete_question", "Can delete question assessment (DS)"),
			]


def get_questions(project=None, category=None, count=getattr(settings, "QUESTION_COUNT", 5)):
	# if project:
	#   count = get_question_count(event=event)

	qsn_list = Question.objects.all().order_by('?').values_list('id', 'random_weight', )
	qsn_id_list = []
	qsn_weight_list = []
	result = []
	for pk, weight in qsn_list:
		qsn_id_list.append(pk)
		qsn_weight_list.append(weight)

	print(qsn_id_list)
	print(qsn_weight_list)

	for _ in range(0, count):
		temp = random.choices(qsn_id_list, qsn_weight_list, k=1)[0]
		temp_idx = qsn_id_list.index(temp)
		qsn_id_list.pop(temp_idx)
		qsn_weight_list.pop(temp_idx)
		result.append(temp)
	
	return Question.objects.filter(id__in=result)


class QuestionChoices(BaseModel):
	question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name="q_choices")
	caption = models.CharField(max_length=100)
	value = models.CharField(max_length=100)
	is_true_answer = models.BooleanField(default=False)
	
	def __str__(self):
		return self.caption

	class Meta:

		permissions = [
				("ds_can_view_choices", "Can view choices assessment (DS)"),
				("ds_can_add_choices", "Can add choices assessment (DS)"),
				("ds_can_edit_choices", "Can edit choices assessment (DS)"),
				("ds_can_delete_choices", "Can delete choices assessment (DS)"),
			]


class AssessmentSession(BaseModel):
	# v1
	user = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True)
	team = models.ForeignKey(Team,on_delete=models.SET_NULL, blank=True, null=True)
	event = models.ForeignKey(Event, on_delete=models.SET_NULL, blank=True, null=True)

	# v2
	participant = models.ForeignKey(Participant, on_delete=models.SET_NULL, blank=True, null=True)
	project = models.ForeignKey(Project, on_delete=models.SET_NULL, blank=True, null=True)

	# v1 = v2
	score = models.IntegerField(default=0)
	start_at = models.DateTimeField(blank=True, null=True) 
	end_at = models.DateTimeField(blank=True, null=True)
	try_again_at = models.DateTimeField(blank=True, null=True)
	passed_at = models.DateTimeField(blank=True, null=True)
	not_passed_at = models.DateTimeField(blank=True, null=True)
	is_recap = models.BooleanField(default=False) 

	def __str__(self):		
		if not self.user:
			return self.participant.name
		return self.user.username

	def save(self, *args, **kwargs):
		hours = get_try_again_hours(self.event, )
		if not self.try_again_at and self.start_at:
			self.try_again_at = self.start_at + timedelta(hours=hours)
		super().save(*args, **kwargs)


class AssessmentDetail(BaseModel):
	session = models.ForeignKey(AssessmentSession, on_delete=models.CASCADE, related_name='a_details')
	question = models.ForeignKey(Question, on_delete=models.CASCADE)
	answer_choices = models.ForeignKey(QuestionChoices, on_delete=models.CASCADE,
													blank=True, null=True)
	answer_text = models.CharField(max_length=200, blank=True, null=True)
	is_answered = models.BooleanField(default=False)
	is_true = models.BooleanField(default=False)

	def save(self, *args, **kwargs):
		# reduce probability of question to be appear after assigned on a session
		if not self.pk:
			qsn = self.question
			qsn.random_weight = models.F('random_weight') - getattr(settings, 'QUESTION_WEIGHT_REDUCER', 0.04)
			qsn.usages = models.F("usages") + 1
			qsn.save()
		super().save(*args, **kwargs)


def create_session(project, email):
    questions = get_questions(category=project.question_category, count=project.total_questions)
    participant = Participant.objects.filter(email=email, project=project).first()
    assessment = AssessmentSession.objects.create(
            created_by=project.created_by,
            participant=participant,
            project=project,
            start_at=timezone.now(),
            end_at=timezone.now() + timedelta(minutes=project.duration_minutes),
        )
    for qsn in questions:
        AssessmentDetail.objects.create(
            created_by=project.created_by,
            session=assessment,
            question=qsn,
        )
    return assessment
