from django.contrib import admin
from .models import (Question, QuestionChoices, AssessmentSession, Participant,
                AssessmentDetail, QuestionCategory, Project, Account, Rules)
from core.libs.django.admin import RawFieldMixins


class RulesInline(admin.TabularInline):
    model = Rules
    extra = 0
    raw_id_fields = ('created_by', 'updated_by', 'deleted_by', )
    list_select_related = True


class QuestionChoicesInline(admin.TabularInline, RawFieldMixins): 
	model = QuestionChoices
	extra = 0
	raw_id_fields = ('created_by', 'updated_by', 'deleted_by', )
	list_select_related = True


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin, RawFieldMixins):
	inlines = (QuestionChoicesInline,)
	raw_id_fields = ('created_by', 'updated_by', 'deleted_by', 'event', 'project')
	list_select_related = True


@admin.register(QuestionChoices)
class QuestionChoicesAdmin(admin.ModelAdmin, RawFieldMixins):
	raw_id_fields = ('created_by', 'updated_by', 'deleted_by', 'question')
	list_select_related = True


@admin.register(Participant)
class ParticipantAdmin(admin.ModelAdmin, RawFieldMixins):
	list_display = ("id", "name", "email", "session_key", "project")
	list_filter = ("project",)
	raw_id_fields = ('created_by', 'updated_by', 'deleted_by', )
	list_select_related = True


class DetailInline(admin.TabularInline, RawFieldMixins):
	model = AssessmentDetail
	extra = 0
	raw_id_fields = ('created_by', 'updated_by', 'deleted_by', )
	list_select_related = True


@admin.register(AssessmentSession)
class AssessmentSessionAdmin(admin.ModelAdmin, RawFieldMixins):
	list_display = ('participant', 'team', 'event', 'score')
	search_fields = ("participant__email", "project__display_name")
	inlines = [DetailInline,]
	raw_id_fields = ('created_by', 'updated_by', 'deleted_by', 'user', 'team', 'event','participant', 'project' )
	list_select_related = True


@admin.register(AssessmentDetail)
class AssessmentDetailAdmin(admin.ModelAdmin, RawFieldMixins):
	list_display = ('id', 'session', 'created_by',)
	raw_id_fields = ('created_by', 'updated_by', 'deleted_by', 'question', 'answer_choices')
	list_select_related = True


@admin.register(QuestionCategory)
class QuestionCategoryAdmin(admin.ModelAdmin):
	list_display = ('display_name', )
	raw_id_fields = ('created_by', 'updated_by', 'deleted_by', )
	list_select_related = True


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ('code', 'display_name', 'description', 'total_questions', 'passed_score', 'duration_minutes' )
    raw_id_fields = ('created_by', 'updated_by', 'deleted_by', 'user' )
    list_select_related = True
    inlines = [RulesInline,]


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
	list_display = ('user', )