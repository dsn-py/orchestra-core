from django.db import models
from django.contrib.auth.models import User


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, 
                                        related_name="%(app_label)s_%(class)s_created_by",)
    updated_at = models.DateTimeField(auto_now=True)
    updated_by = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True,
                                        related_name="%(app_label)s_%(class)s_updated_by",)
    deleted_at = models.DateTimeField(blank=True, null=True)
    deleted_by = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True,
                                        related_name="%(app_label)s_%(class)s_deleted_by",)
    
    class Meta:
        abstract = True


class BaseAddressModel(BaseModel):
    # basic profile
    address = models.TextField(blank=True, null=True)
    city = models.CharField(max_length=50,blank=True, null=True)
    province = models.CharField(max_length=50,blank=True, null=True)
    postcode = models.CharField(max_length=10,blank=True, null=True)
    country = models.CharField(max_length=3, default="IDN")

    class Meta:
        abstract = True


class AddressMixins(object):
    # DEPRECATED
    address = models.TextField(blank=True, null=True)
    city = models.CharField(max_length=50)
    province = models.CharField(max_length=50)
    postcode = models.CharField(max_length=10)
    country = models.CharField(max_length=3)

    class Meta:
        abstract = True