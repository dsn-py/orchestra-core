from django.contrib import admin
from .models import ClassMaterial


class MaterialAdmin(admin.ModelAdmin):
	list_display = ('event', 'created_at', 'title', 'group')
	list_filter = ('event', 'type')

admin.site.register(ClassMaterial, MaterialAdmin)