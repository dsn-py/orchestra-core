from django.db import models
from core.dbs.base.models import BaseModel
from core.dbs.master.models import Event
from core.dbs.participant.models import EventGroup as Group
from core.libs import storage


class ClassMaterial(BaseModel):
	MATERIAL_TYPE_CHOICES = (
		('document', 'Document'),
		('link', 'Document Link'),
		('stream', 'Movie Streaming'),
	)
	event = models.ForeignKey(Event, on_delete=models.CASCADE)
	title = models.CharField(max_length=200)
	cover = models.ImageField(blank=True, null=True, max_length=300,
							storage=storage.STORAGE_MATERIAL)
	file = models.FileField(blank=True, null=True, max_length=300,
							storage=storage.STORAGE_MATERIAL)
	description = models.TextField(blank=True, )
	drive_url = models.URLField(blank=True, null=True)
	type = models.CharField(max_length=20, choices=MATERIAL_TYPE_CHOICES, 
											default="document")
	group = models.ForeignKey(Group, on_delete=models.CASCADE,
									blank=True, null=True)
	priority = models.SmallIntegerField(default=100)
	
	def __str__(self):
		return "{}:{}".format(self.event.short_name, self.title)

	class Meta:
		ordering = ['-created_at',]

		permissions = [
            ("ds_can_view_material", "Can view material (DS)"),
            ("ds_can_add_material", "Can add material (DS)"),
            ("ds_can_edit_material", "Can edit material (DS)"),
            ("ds_can_delete_material", "Can delete material (DS)"),
        ]
