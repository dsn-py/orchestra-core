from django.contrib import admin
from .models import Tasklist, Requirement, Response, CheckList, ResponseAttachment


class RequirementInline(admin.TabularInline):
	model = Requirement
	extra = 1

class AttachmentInline(admin.TabularInline):
	model = ResponseAttachment
	extra = 1

@admin.register(Tasklist)
class TasklistAdmin(admin.ModelAdmin):
	list_display = ('id', 'event', 'group', 'title', 'begin_date', 'end_date')
	inlines = [RequirementInline,]


@admin.register(Requirement)
class RequirementAdmin(admin.ModelAdmin):
	list_display = ('id', 'tasklist', 'caption')


class ChecklistInline(admin.StackedInline):
		model = CheckList
		fields = ("id", "requirement", "checked_at")

@admin.register(Response)
class ResponseAdmin(admin.ModelAdmin):
	list_display = ('id', 'user', 'team', 'tasklist', 'created_at', 'checked_at')
	inlines = [ChecklistInline, AttachmentInline]


@admin.register(CheckList)
class ChecklistAdmin(admin.ModelAdmin):
	list_display = ('id', 'user', 'tasklist', 'requirement', 'checked_at')	

