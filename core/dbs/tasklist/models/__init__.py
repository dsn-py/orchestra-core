from django.db import models
from django.utils.text import slugify
from django.contrib.auth.models import User
from core.libs import storage
from core.dbs.base.models import BaseModel
from core.dbs.master.models import Event
from core.dbs.participant.models import EventGroup as Group, Team
from core.dbs.activity.models import Activity


class Tasklist(BaseModel):
	event = models.ForeignKey(Event, on_delete=models.CASCADE)
	group = models.ForeignKey(Group, on_delete=models.CASCADE,
									blank=True, null=True)
	title = models.CharField(max_length=200)
	file = models.FileField(storage=storage.STORAGE_TASKLIST,
											blank=True, null=True)
	description = models.TextField()
	begin_date = models.DateTimeField()
	end_date = models.DateTimeField()

	team = models.ManyToManyField(Team, blank=True)
	
	def __str__(self):
		return "{}-{}:{}".format(self.id, self.event.short_name, self.title)

	class Meta:

		permissions = [
				("ds_can_view_tasklist", "Can view tasklist (DS)"),
				("ds_can_add_tasklist", "Can add tasklist (DS)"),
				("ds_can_edit_tasklist", "Can edit tasklist (DS)"),
				("ds_can_delete_tasklist", "Can delete tasklist (DS)"),
			]


class Requirement(BaseModel):
	tasklist = models.ForeignKey(Tasklist, on_delete=models.CASCADE)
	caption = models.CharField(max_length=200)

	def __str__(self):
		return self.caption


class Response(BaseModel):
	user = models.ForeignKey(User, on_delete=models.CASCADE, 
							related_name="tasklist_response_user")
	team = models.ForeignKey(Team, on_delete=models.CASCADE, blank=True, null=True,
							related_name="tasklist_response_team")
	tasklist = models.ForeignKey(Tasklist, on_delete=models.CASCADE)
	text = models.TextField(blank=True)
	drive_url = models.URLField(blank=True, null=True)
	file = models.FileField(storage=storage.STORAGE_TASKLIST,
											blank=True, null=True)
	checked_by = models.ForeignKey(User,on_delete=models.CASCADE, 
							blank=True, null=True,
							related_name="tasklist_response_checked_by")
	checked_at = models.DateTimeField(blank=True, null=True)

	def __str__(self):
		return self.user.username

	def save(self, *args, **kwargs):
		super().save(*args, **kwargs)
		activity = Activity()
		activity.event=self.tasklist.event
		activity.content_object=self
		activity.created_by=self.user
		activity.model="tasklist"
		activity.user=self.tasklist.created_by
		activity.save()


class ResponseAttachment(BaseModel):
	response = models.ForeignKey(Response, on_delete=models.CASCADE,)
	caption = models.CharField(max_length=128, blank=True, null=True)
	url = models.URLField(blank=True, null=True)
	file = models.FileField(storage=storage.STORAGE_TASKLIST,
											blank=True, null=True)
	def __str__(self):
		return self.caption


class CheckList(BaseModel):
	tasklist = models.ForeignKey(Tasklist, on_delete=models.CASCADE)
	response = models.ForeignKey(Response, on_delete=models.CASCADE, )
	user = models.ForeignKey(User, on_delete=models.CASCADE, 
							related_name="tasklist_checked_user")
	requirement = models.ForeignKey(Requirement, on_delete=models.CASCADE)
	checked_at = models.DateTimeField(blank=True, null=True)

	def __str__(self):
		return self.user.username