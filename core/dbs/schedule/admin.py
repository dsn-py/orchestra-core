from django.contrib import admin
from .models import MainCalendar, UserCalendar
from core.libs.django.admin import RawFieldMixins


class EventCalendarAdmin(admin.ModelAdmin, RawFieldMixins):
    list_display = ( 'title', 'id', 'event',  'event_at', 'start_time', 'created_by')
    search_fields = ('event__display_name__icontains', 'title__icontains')
    list_filter = ("event",)

    raw_id_fields = ('event', 'created_by', 'updated_by', 'deleted_by', 'group')
    list_select_related = True




class UserCalendarAdmin(admin.ModelAdmin):
    list_display = ('owned_by', 'event', 'title', 'event_at', 'place' )
    search_fields = ('owned_by__username__icontains', 
                                            'event__display_name__icontains', 
                                            'title__icontains')

admin.site.register(MainCalendar, EventCalendarAdmin)
admin.site.register(UserCalendar, UserCalendarAdmin)