import pytz
import datetime
from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from core.dbs.base.models import BaseModel
from core.dbs.master.models import Event
from core.dbs.participant.models import EventGroup


class MainCalendar(BaseModel):
	event = models.ForeignKey(Event, on_delete=models.CASCADE)
	group = models.ForeignKey(EventGroup, on_delete=models.CASCADE,
											blank=True, null=True)
	event_at = models.DateTimeField()
	start_time = models.TimeField(blank=True, null=True)
	end_time = models.TimeField(blank=True, null=True)
	place = models.TextField(blank=True, null=True)
	# copied to all user
	is_main_event = models.BooleanField(default=False)
	title = models.CharField(max_length=200)
	description = models.TextField(blank=True)

	def __str__(self):
		return self.title

	def save(self, *args, **kwargs):
		tz = pytz.timezone(settings.TIME_ZONE)
		event_at = self.event_at
		start_time = self.start_time
		new_event_date = datetime.datetime(
			event_at.year,
			event_at.month,
			event_at.day,
			start_time.hour,
			start_time.minute,
		)
		self.event_at = tz.localize(new_event_date)
		super().save(*args, **kwargs)

	class Meta:
		ordering = ['event', 'event_at',]

		permissions = [
			("ds_can_view_calendar", "Can view calendar (DS)"),
			("ds_can_add_calendar", "Can add calendar (DS)"),
			("ds_can_edit_calendar", "Can edit calendar (DS)"),
			("ds_can_delete_calendar", "Can delete calendar (DS)"),
		]

def maincalendar_export(groupmember):
	# distribute calendar to attendee
	main = MainCalendar.objects.filter(
		event=groupmember.group.event,
		group=None,
	)
	for item in main:
		UserCalendar.objects.get_or_create(
			owned_by=groupmember.user,
			created_by=groupmember.user,
			event=groupmember.group.event,
			start_time=item.start_time,
			end_time=item.end_time,
			parent=item,
			place=item.place,
			event_at=item.event_at,
			title=item.title,
			description=item.description,
		)

	group = MainCalendar.objects.filter(
		event=groupmember.group.event,
		group=groupmember.group,
	)
	for item in group:
		UserCalendar.objects.get_or_create(
			owned_by=groupmember.user,
			created_by=groupmember.user,
			event=groupmember.group.event,
			start_time=item.start_time,
			end_time=item.end_time,
			parent=item,
			place=item.place,
			event_at=item.event_at,
			title=item.title,
			description=item.description,
		)


class UserCalendar(BaseModel):
	event = models.ForeignKey(Event, on_delete=models.CASCADE)
	parent = models.ForeignKey(MainCalendar, on_delete=models.CASCADE, 
												blank=True, null=True)
	owned_by = models.ForeignKey(User, on_delete=models.CASCADE, 
							related_name="schedule_usercalendar_user")
	place = models.TextField(blank=True, null=True)
	event_at = models.DateTimeField()
	start_time = models.TimeField(blank=True, null=True)
	end_time = models.TimeField(blank=True, null=True)
	title = models.CharField(max_length=200)
	description = models.TextField(blank=True)
	is_attend = models.NullBooleanField()

	def __str__(self):
		return self.title

	class Meta:
		ordering = ['event', 'event_at',]