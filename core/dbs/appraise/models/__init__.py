from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator
from core.dbs.base.models import BaseModel
from core.dbs.master.models import Event


class Aspect(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE, 
                            related_name="appraise_aspect_event")
    name = models.CharField(max_length=128, )
    description = models.CharField(max_length=128, blank=True, null=True)
    weight = models.FloatField(default=0, validators=[
                               MaxValueValidator(1), MinValueValidator(0)])

    def __str__(self):
        return self.name or self.id

    class Meta:
        unique_together = ('event', 'name')


class Appraise(BaseModel):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE,
                                related_name="appraise_appraise_user")
    mentor = models.ForeignKey(User, on_delete=models.CASCADE,
                                related_name="appraise_appraise_mentor")
    total = models.FloatField(default=0)

    def __str__(self):
        return "{}::{}".format(self.user.username, self.total)

    class Meta:
        unique_together = ('event', 'user', 'mentor')


class AppraiseDetail(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    appraise = models.ForeignKey(Appraise, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE,
                                related_name="appraise_appraisedetail_user")
    mentor = models.ForeignKey(User, on_delete=models.CASCADE,
                                related_name="appraise_appraisedetail_mentor")
    aspect = models.ForeignKey(Aspect, on_delete=models.CASCADE)
    score = models.SmallIntegerField(
        blank=True,
        null=True,
        validators=[MaxValueValidator(100), MinValueValidator(0)]
    )
    total = models.FloatField(default=0)

    def __str__(self):
        return str(self.total)

    class Meta:
        unique_together = ( 'event', 'user', 'mentor', 'aspect' )

    def save(self, *args, **kwargs):
        self.total = round(self.score * self.aspect.weight, 2)
        super().save(*args, **kwargs)
        
        detail = AppraiseDetail.objects.filter(
            appraise=self.appraise,
            user=self.user,
        ).aggregate(models.Sum('total'))
        score, created = Appraise.objects.get_or_create(
            event=self.event,
            user=self.user,
            mentor=self.mentor,
        )
        score.total = round(detail["total__sum"], 2)
        score.save()

    


