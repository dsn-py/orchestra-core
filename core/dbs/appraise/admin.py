from django.contrib import admin
from .models import Aspect, Appraise, AppraiseDetail


@admin.register(Aspect)
class AspectAdmin(admin.ModelAdmin):
	list_display = ('id', 'event', 'name', 'weight')


@admin.register(Appraise)
class AppraiseAdmin(admin.ModelAdmin):
	list_display = ('id', 'event', 'user', 'mentor', 'total')


@admin.register(AppraiseDetail)
class AppraiseDetailAdmin(admin.ModelAdmin):
	list_display = ('id', 'event', 'user', 'mentor', 'total', 'aspect')
