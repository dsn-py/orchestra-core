import csv
from django.http import HttpResponse

class ExportCsvMixin:
    def export_as_csv(self, request, queryset):
        row = None
        meta = self.model._meta
        field_names = [field.name for field in meta.fields]
        property_names = [name for name in dir(self.model) if isinstance(getattr(self.model, name), property)]
        all_field_names = field_names + property_names

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(meta)
        writer = csv.writer(response)

        writer.writerow(all_field_names)
        for obj in queryset:
            row = writer.writerow([getattr(obj, field) for field in all_field_names])

        return response

    export_as_csv.short_description = "Export Selected"