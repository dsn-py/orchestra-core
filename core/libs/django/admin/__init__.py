from itertools import chain
from django.conf import settings
from django.contrib.auth import get_user_model


# social auth legacy
# SETTING_PREFIX = 'SOCIAL_AUTH'

# def to_setting_name(*names):
#     return '_'.join([name.upper().replace('-', '_') for name in names if name])


# def setting_name(*names):
    # return to_setting_name(*((SETTING_PREFIX,) + names))
# end legacy

class RawFieldMixins(object):
    def get_search_fields(self, request=None):
        # search_fields = getattr(
        #     settings, setting_name('ADMIN_USER_SEARCH_FIELDS'), None
        # )
        search_fields = None
        if search_fields is None:
            _User = get_user_model()
            username = getattr(_User, 'USERNAME_FIELD', None) or \
                       hasattr(_User, 'username') and 'username' or \
                       None
            fieldnames = ('first_name', 'last_name', 'email', username)
            all_names = self._get_all_field_names(_User._meta)
            search_fields = [name for name in fieldnames
                                if name and name in all_names]
        return ['user__' + name for name in search_fields]

    @staticmethod
    def _get_all_field_names(model):
        names = chain.from_iterable(
            (field.name, field.attname)
                if hasattr(field, 'attname') else (field.name,)
            for field in model.get_fields()
            # For complete backwards compatibility, you may want to exclude
            # GenericForeignKey from the results.
            if not (field.many_to_one and field.related_model is None)
        )
        return list(set(names))

