from django.contrib.auth.models import Permission


def has_perm(user, perm_name, request_method_lower='get'):
    
    if type(perm_name).__name__=='list':
        for item in perm_name:
            if item[1] != request_method_lower:
                continue
            perm_name = item[0]

    if type(perm_name).__name__ == 'str':
        per = Permission.objects.filter(codename=perm_name).first()
        if not per:
            return False, perm_name
        if per in user.user_permissions.all():
            return True, perm_name
        return False, perm_name
    elif type(perm_name).__name__ == 'list':
        per = Permission.objects.filter(codename__in=perm_name).first()
        if not per:
            return False, perm_name
        if per in user.user_permissions.all():
            return True, perm_name
        return False, perm_name
