import io
import string
import xlsxwriter
from django.http import HttpResponse

class WorksheetGenerator(object):
    @staticmethod
    def colnum_string(n):
        string = ""
        while n > 0:
            n, remainder = divmod(n - 1, 26)
            string = chr(65 + remainder) + string
        return string

    @staticmethod
    def colstring_num(col):
        num = 0
        for c in col:
            if c in string.ascii_letters:
                num = num * 26 + (ord(c.upper()) - ord('A')) + 1
        return num

    def _generate_web_init(self,):
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output)
        worksheet = workbook.add_worksheet(name=self.worksheet_name)
        self.output = output
        self.workbook = workbook
        self.worksheet = worksheet

    def _generate_local_init(self,):
        workbook = xlsxwriter.Workbook(self.filename)
        worksheet = workbook.add_worksheet(name=self.worksheet_name)
        self.output = None
        self.workbook = workbook
        self.worksheet = worksheet

    def __init__(self, filename, worksheet_name="WS01", web_output=True):
        self.filename = filename
        self.worksheet_name = worksheet_name
        self.web_output = web_output
        self._row = 0

        if web_output:
            self._generate_web_init()
        else:
            self._generate_local_init()

    def set_column_format(self, column_start, column_end, width=10, format_dict=None):
        self.worksheet.set_column(column_start, column_end, width=width, cell_format=format_dict)
        return True

    def _define_cell_format(self, configuration={}):
        cell_format = self.workbook.add_format()
        cell_format.set_bold()
        cell_format.set_bg_color('silver')
        return cell_format

    def set_row_data(self, data_lst, to_next_row=True, cell_config={}):
        col = 0
        for col, item in enumerate(data_lst):
            self.worksheet.write(self._row, col, data_lst[col], cell_config)

        if to_next_row:
            self._row += 1
        pass

    def finalize(self):
        self.workbook.close()
        if self.web_output:
            self.output.seek(0)
        return True

    def http_response(self,):
        filename = self.filename
        response = HttpResponse(
            self.output,
            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        )
        response['Content-Disposition'] = 'attachment; filename=%s' % filename