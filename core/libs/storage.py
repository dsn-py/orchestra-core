from django.conf import settings
from django.core.files.storage import FileSystemStorage
from core.libs.cloud_files.s3 import *

def generate_name(instance, filename):
    pass

ROOT_URL = ""

# BASE_URL = "https://1c09eaf92556a648da8e-9c16e1a40146508189f4ff3eaa9a5a82.ssl.cf6.rackcdn.com"
BASE_URL = "https://{}.{}".format(s3_container, s3_base_endpoint_url)


STORAGE_USER_PHOTO = FileSystemStorage(location=settings.MEDIA_ROOT, 
                    base_url='%smedia/user/photo' % settings.BASE_URL)

STORAGE_PRODUCT = FileSystemStorage(location=settings.MEDIA_ROOT, 
                    base_url='%smedia/company/product' % settings.BASE_URL)

STORAGE_MATERIAL = FileSystemStorage(location=settings.MEDIA_ROOT, 
                    base_url='%smedia/material' % settings.BASE_URL)

STORAGE_FORUM = FileSystemStorage(location=settings.MEDIA_ROOT, 
                    base_url='%smedia/forum' % settings.BASE_URL)

STORAGE_TASKLIST = FileSystemStorage(location=settings.MEDIA_ROOT, 
                    base_url='%smedia/tasklist' % settings.BASE_URL)
STORAGE_IDEATION = FileSystemStorage(location=settings.MEDIA_ROOT, 
                    base_url='%smedia/ideation' % settings.BASE_URL)
STORAGE_COMPANY = FileSystemStorage(location=settings.MEDIA_ROOT, 
                    base_url='%smedia/company' % settings.BASE_URL)
STORAGE_EVENT = FileSystemStorage(location=settings.MEDIA_ROOT, 
                    base_url='%smedia/event' % settings.BASE_URL)
STORAGE_ASSESSMENT = FileSystemStorage(location=settings.MEDIA_ROOT, 
                    base_url='%smedia/assessment' % settings.BASE_URL)
STORAGE_MASTER = FileSystemStorage(location=settings.MEDIA_ROOT, 
                    base_url='%smedia/master' % settings.BASE_URL)


if settings.USE_S3:
    STORAGE_USER_PHOTO = S3FileStorage(
        location = "%suser/photo/" % ROOT_URL,
        base_url = BASE_URL,
        purpose = "USER_PHOTO"
    )
    STORAGE_PRODUCT = S3FileStorage(
        location = "%scompany/product/" % ROOT_URL,
        base_url = BASE_URL,
        purpose = "PRODUCT_PHOTO"
    )
    STORAGE_MATERIAL = S3FileStorage(
        location = "%smaterial/" % ROOT_URL,
        base_url = BASE_URL,
        purpose = "MATERIAL"
    )
    STORAGE_FORUM = S3FileStorage(
        location = "%sforum/" % ROOT_URL,
        base_url = BASE_URL,
        purpose = "FORUM"
    )
    STORAGE_TASKLIST = S3FileStorage(
        location = "%stasklist/" % ROOT_URL,
        base_url = BASE_URL,
        purpose = "TASKLIST"
    )
    STORAGE_IDEATION = S3FileStorage(
        location = "%sideation/" % ROOT_URL,
        base_url = BASE_URL,
        purpose = "IDEATION"
    )
    STORAGE_COMPANY = S3FileStorage(
        location = "%scompany/" % ROOT_URL,
        base_url = BASE_URL,
        purpose = "COMPANY"
    )
    STORAGE_EVENT = S3FileStorage(
        location = "%sevent/" % ROOT_URL,
        base_url = BASE_URL,
        purpose = "EVENT"
    )
    STORAGE_ASSESSMENT = S3FileStorage(
        location = "%sassessment/" % ROOT_URL,
        base_url = BASE_URL,
        purpose = "ASSESSMENT"
    )
    STORAGE_MASTER = S3FileStorage(
        location = "%smaster/" % ROOT_URL,
        base_url = BASE_URL,
        purpose = "MASTER"
    )
