WINNER_CHOICES = (
	(1, '1st Winner'),
	(2, '2nd Winner'),
	(3, '3rd Winner'),
	(99, 'N/A')
)

TEAM_STATUS_CHOICES = (
	('waiting', 'Waiting for Approval'),
	('approved', 'Approved'),
	('rejected', 'Rejected')
)

IDEA_STATUS_CHOICES = (
	('waiting', 'Waiting for qualifications'),
	('qualified', 'Qualified'),
	('rejected', 'Rejected')
)