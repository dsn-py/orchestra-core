EVENT_TYPE_CHOICES = (
	('hackathon', 'Hackathon'),
	('incubation', 'Incubation'),
	('other', 'Other')
)

ATTEND_TYPE_CHOICES = (
	('visitor', 'Public Visitor'),
	('participant', 'Participant to Event'),
	('judge', 'Judge of the Event'),
	('mentor', 'Mentor of the Event'),
	('executive', 'Executive'), 
	('superuser', 'Super User'), 
)

ROADMAP_TYPE_CHOICES = (
	('roadshow', 'Roadshow'),
	('announcement', 'Announcement'),
	('main_event', 'Main Event'),
	('notset', 'Not Set')
)