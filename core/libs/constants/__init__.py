from .events import *
from .tournament import *
from .forum import *


ACTIVITY_SIGNAL_CHOICES = (
	('resume', 'Resume Back To Apps'),
	('attend', 'Attend offline event'),
	('material', 'Open Material'),
	('submit-task', 'Response to a Tasklist'),
	('submit-progress', 'Submit a Progress'),
	('open-topic', 'Add Forum Topic'),
	('comment-topic', 'Comment a Topic'),
	('discuss', 'Open Discussion with Mentor')
)


INVOLVEMENT_TYPE_CHOICES = (
	('part', 'Part Time'),
	('full', 'Full Time'),
)


COMPANY_TYPE_CHOICES = (
	('corporate', 'Corporate'),
	('startup', 'Startup'),
	('common', 'Common'),
)


COMPANY_STATUS_CHOICES = (
	('operated', 'Operated'),
	('not-operated', 'Not Operated'),
	('acquired', 'Acquisition/Acquired' ),
)


BUSINESS_SCOPE_CHOICES = (
	('b2c', 'B2C'),
	('b2b', 'B2B'),
	('c2c', 'C2C'),
	('b2g', 'B2G'),
	('other', 'Other'),
)


FUNDING_ROUND_CHOICES = (
	('bootstrap', 'Bootstrap or Self funding'),
	('competition', 'Won a competition'),
	('angel', 'Angel'),
	('pre_seed', 'Pre-seed'),
	('seed', 'Seed'),
	('venture', 'Venture, Series unknown'),
	('pre_series_a', 'Pre-Series A'),
	('series_a', 'Series A'),
	('series_b', 'Series B'),
	('series_c', 'Series C'),
	('series_d', 'Series D'),
	('equity_crowdfunding', 'Equity Crowdfunding'),
	('product_crowdfunding', 'Product Crowdfunding'),
	('equity', 'Private Equity - IPO'),
	('debt', 'Debt Financing or Bank'),
	('grant', 'Grant (Hibah)'),
	('ico', 'Initial Coin Offering'),
	('other', 'Other'),
)


POSITION_LEVEL = (
	('staff-associate', 'Associate or Junior Staff'),
	('staff-intermediate', 'Intermediate Staff'),
	('staff-senior', 'Senior Staff'),
	('management-manager', 'Manager'),
	('management-senior', 'Senior Manager'),
	('management-advisor','Advisor'),
	('management-senior-advisor','Senior Advisor'),
	('mid-management-director', 'Director'),
	('mid-management-senior-director', 'Senior Director'),
	('executive-management-executive', 'Executive'),
	('executive-management-senior-executive', 'Senior Executive'),
)


MEDIA_CHOICES = (
	('website', 'Website'),
	('tweet', 'Twitter'),
	('fb', 'Facebook'),
	('ig', 'Instagram'),
	('marketplace', 'Marketplace or Online Shop')
)


FOUNDER_TYPE_CHOICES = (
	('founder', 'Founder and Co Founder'),
	('founding-member', 'Founding Member; non official Founder or not registered in company. '),
	('core-employee', 'First Employee'),
	('advisor', 'Advisor')
)