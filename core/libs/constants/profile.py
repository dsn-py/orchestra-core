PROFILE_GENDER_CHOICES = (
	('male', 'Male'),
	('female', 'Female'),
	('notset', 'Not Set')
)

PROFILE_DEGREE_CHOICES = (
	('sma', 'SMA'),
	('smk', 'SMK'),
	('d3', 'D3'),
	('s1', 'S1'),
	('s2', 'S2'),
	('notset', 'Not Set')
)