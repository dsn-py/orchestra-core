import requests
from django.conf import settings

class QiscusBase(object):
	@property
	def secret(self):
		return getattr(settings, 'QISCUS_SECRET_KEY', '07b7a88a65ed25a7c22613e86a73bb20')

	@property
	def headers(self):
		headers = {
			'Content-Type': 'application/json',
			'QISCUS_SDK_SECRET': self.secret
		}
		return headers

	@property
	def base_url(self):
		base_url = "https://incubator-teqp1kynufq.qiscus.com/api/v2.1/rest/"
		return base_url
	
	def get(self, url, params="", ):
		long_url = "{}{}".format(self.base_url, url)
		final_url = ""

		if params:
			final_url = "{}?{}".format(long_url, params)
		else:
			final_url = long_url

		resp = requests.get(
				final_url, 
				headers=self.headers
			)
		return resp.content, resp.status_code


	def post(self, url, payload):
		long_url = "{}{}".format(self.base_url, url)
		resp = requests.post(
				long_url,
				headers=self.headers,
				data=payload
			)
		return resp.content, resp.status_code


class Qiscus(object):
	def get_user_profile(self, email):
		base = QiscusBase()
		return base.get("user_profile", params="user_id={}".format(email))

	# def login_or_register(self, ):
	# 	payload = {
	# 		"user_id": "guest99@qiscus.com",
	# 		"password": "password","username": "QISCUS demo user","avatar_url": "https://image.url/image.jpeg"}

