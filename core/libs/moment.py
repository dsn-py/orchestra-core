from math import ceil
from datetime import datetime, timedelta


def to_timestamp(now, epoch=None):
    if not epoch:
        epoch = datetime(1970, 1, 1)
    return int((now - epoch).total_seconds())


def get_next_monday(today):
    today = today.date()
    return today + timedelta(days=-today.weekday(), weeks=1)


def get_last_monday(today, ):
    today = today.date()
    return today - timedelta(days=today.weekday())


def week_of_month(dt):
    """ 
    Returns the week of the month for the specified date.
    """
    first_day = dt.replace(day=1)

    dom = dt.day
    adjusted_dom = dom + first_day.weekday()

    return int(ceil(adjusted_dom/7.0))