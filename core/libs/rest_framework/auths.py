"""
Provides various authentication policies.
"""
from __future__ import unicode_literals

import json
import requests
import tempfile

from django.core import files
from django.conf import settings
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from rest_framework.authentication import BaseAuthentication, get_authorization_header
from rest_framework import exceptions


def get_sso_user(token):
        base_url = getattr(settings, "ORCHESTRA_SSO_URL", "https://sso.orchestra.co.id/")
        url = base_url + "apis/account/me/"
        auth = {"Authorization": "token {}".format(token,)}
        response = requests.get(url, headers=auth)
        return json.loads(response.text), response.status_code


class TokenAuthentication(BaseAuthentication):
    """
    Simple token based authentication.

    Clients should authenticate by passing the token key in the "Authorization"
    HTTP header, prepended with the string "Token ".  For example:

        Authorization: Token 401f7ac837da42b97f613d789819ff93537bee6a
    """

    keyword = 'Token'
    model = None

    def get_model(self):
        if self.model is not None:
            return self.model
        from rest_framework.authtoken.models import Token
        return Token

    """
    A custom token model may be used, but must have the following properties.

    * key -- The string identifying the token
    * user -- The user to which the token belongs
    """

    def authenticate(self, request):
        auth = get_authorization_header(request).split()

        if not auth or auth[0].lower() != self.keyword.lower().encode():
            return None

        if len(auth) == 1:
            msg = _('Invalid token header. No credentials provided.')
            raise exceptions.AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = _('Invalid token header. Token string should not contain spaces.')
            raise exceptions.AuthenticationFailed(msg)

        try:
            token = auth[1].decode()
        except UnicodeError:
            msg = _('Invalid token header. Token string should not contain invalid characters.')
            raise exceptions.AuthenticationFailed(msg)

        return self.authenticate_credentials(token)


    def authenticate_credentials(self, key):
        model = self.get_model()
        user_json = None
        user = None
        token = None
        try:
            token = model.objects.select_related('user').get(key=key)
        except model.DoesNotExist:
            user_json, status_code = get_sso_user(key)
            if status_code != 200:
                raise exceptions.AuthenticationFailed(_('Invalid token.'))

        if user_json:
            orc_user = User.objects.filter(email=user_json['email']).first()
            if not orc_user:
                orc_user = User.objects.create(
                    username=user_json['username'],
                    email=user_json['email'],
                )
                orc_profile = orc_user.profile
                orc_profile.full_name = user_json['full_name']
                orc_profile.phone_number = user_json['profile']['phone_number'],
                orc_profile.birthday = user_json['profile']['birthday']
                orc_profile.bio = user_json['profile']['bio']

                # Steam the image from the url
                if user_json['profile']['photo']:
                    image_url = user_json['profile']['photo']
                    request = requests.get(image_url, stream=True)

                    # Was the request OK?
                    if request.status_code == requests.codes.ok:
                    # Get the filename from the url, used for saving later
                        file_name = image_url.split('/')[-1]

                        # Create a temporary file
                        lf = tempfile.NamedTemporaryFile()

                        # Read the streamed image in sections
                        for block in request.iter_content(1024 * 8):
                            # If no more file then stop
                            if not block:
                                break
                            # Write image block to temporary file
                            lf.write(block)

                        # Save the temporary image to the model#
                        # This saves the model so be sure that is it valid
                        orc_profile.photo.save(file_name, files.File(lf))
                orc_profile.save()
            user = orc_user
        else:
            user = token.user

        if token and not token.user.is_active:
            raise exceptions.AuthenticationFailed(_('User inactive or deleted.'))

        return (user, token)

    def authenticate_header(self, request):
        return self.keyword



def authenticate_credentials(key, model=None):
    if not model:
        from rest_framework.authtoken.models import Token
        model = Token
    user_json = None
    user = None
    token = None
    try:
        token = model.objects.select_related('user').get(key=key)
    except model.DoesNotExist:
        user_json, status_code = get_sso_user(key)
        if status_code != 200:
            raise exceptions.AuthenticationFailed(_('Invalid token.'))

    if user_json:
        orc_user = User.objects.filter(email=user_json['email']).first()
        if not orc_user:
            orc_user = User.objects.create(
                username=user_json['username'],
                email=user_json['email'],
            )
            orc_profile = orc_user.profile
            orc_profile.full_name = user_json['full_name']
            orc_profile.phone_number = user_json['profile']['phone_number'],
            orc_profile.birthday = user_json['profile']['birthday']
            orc_profile.bio = user_json['profile']['bio']

            # Steam the image from the url
            if user_json['profile']['photo']:
                image_url = user_json['profile']['photo']
                request = requests.get(image_url, stream=True)

                # Was the request OK?
                if request.status_code == requests.codes.ok:
                # Get the filename from the url, used for saving later
                    file_name = image_url.split('/')[-1]

                    # Create a temporary file
                    lf = tempfile.NamedTemporaryFile()

                    # Read the streamed image in sections
                    for block in request.iter_content(1024 * 8):
                        # If no more file then stop
                        if not block:
                            break
                        # Write image block to temporary file
                        lf.write(block)

                    # Save the temporary image to the model#
                    # This saves the model so be sure that is it valid
                    orc_profile.photo.save(file_name, files.File(lf))
            orc_profile.save()
        user = orc_user
    else:
        user = token.user

    if token and not token.user.is_active:
        raise exceptions.AuthenticationFailed(_('User inactive or deleted.'))
    return user
