from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.mixins import (CreateModelMixin as OriginalCreateMixins,
                                    UpdateModelMixin as OriginalUpdateMixins)


class CreateModelMixin(OriginalCreateMixins):
    """
    Create a model instance.
    """
    def perform_create(self, serializer):
        user = self.request.user
        serializer.validated_data['created_by'] = user
        serializer.save()


class UpdateModelMixin(OriginalUpdateMixins):
    """
    Update a model instance.
    """
    def perform_update(self,serializer):
        obj = serializer.save()
        obj.updated_by = self.request.user
        obj.save()


class ContextMixin(object):
    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request'] = self.request

        if not self.request.user.is_anonymous:
            self.request.user.profile.set_request(self.request)
        return context


class MultipleFieldLookupMixin(object):
    """
    Apply this mixin to any view or viewset to get multiple field filtering
    based on a `lookup_fields` attribute, instead of the default single field filtering.
    """
    def get_object(self):
        queryset = self.get_queryset()             # Get the base queryset
        queryset = self.filter_queryset(queryset)  # Apply any filter backends
        filter = {}
        for field in self.lookup_fields:
            if self.kwargs[field]: # Ignore empty fields.
                filter[field] = self.kwargs[field]
        obj = get_object_or_404(queryset, **filter)  # Lookup the object
        self.check_object_permissions(self.request, obj)
        return obj