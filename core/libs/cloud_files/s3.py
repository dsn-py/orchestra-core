import os
import boto3
import magic
from mimetypes import MimeTypes
from django.conf import settings
from django.utils import timezone
from django.core.files.base import ContentFile
from django.core.files.storage import FileSystemStorage
from django.utils.crypto import get_random_string
from django.utils.deconstruct import deconstructible
from django.utils.encoding import force_text as force_unicode, smart_str
from django.utils.functional import cached_property


s3_access_key = getattr(settings, "S3_ACCESS_KEY", '2741ad3b82b06a76de93')
s3_secret = getattr(settings, "S3_SECRET", 'PsafascFMChqX1wGqIbmUm70mZCVJZudvUSV8IJA')
s3_region = getattr(settings, "S3_REGION", 'idn')
s3_container = getattr(settings, "S3_CONTAINER", 'orchestra')
s3_base_endpoint_url = getattr(settings, "S3_ENDPOINT_URL", "nos.wjv-1.neo.id")
s3_endpoint_url = "https://" + s3_base_endpoint_url 

client = boto3.client('s3', 
    aws_access_key_id=s3_access_key, 
    aws_secret_access_key=s3_secret, 
    region_name=s3_region,
    endpoint_url=s3_endpoint_url,
)

@deconstructible
class S3FileStorage(FileSystemStorage):
    def __init__(self, container=None, *args, **kwargs):
        if not settings.USE_S3:
            return None

        default_container = s3_container
        container = default_container if not container else container
        self.purpose = kwargs.pop("purpose")
        self.container = client.head_bucket(Bucket=s3_container)
        super(S3FileStorage, self).__init__(*args, **kwargs)

    def _open(self, name, mode='rb'):
        # This must return a File object
        response = client.get_object(Bucket=s3_container, Key=name)
        return ContentFile(response['Body'].read())

    def _save(self, name, content):
        # name = remote path + new_name
        # Should return the actual name of name of the file saved (usually the name passed in, 
        # but if the storage needs to change the file name return the new name instead).
        # new_name = self.get_valid_name(name)
        content_type = None
        try:
            content_type = content.content_type
        except:
            pass

        if not content_type:
            try:
                mime = MimeTypes()
                mime_type = mime.guess_type(content.file)
                content_type = mime_type
            except:
                pass

        if not content_type:
            client.upload_fileobj(content.file, s3_container, name, ExtraArgs={
                                'ACL':'public-read',}
                        )
        else:
            client.upload_fileobj(content.file, s3_container, name, ExtraArgs={
                                    'ACL':'public-read',
                                    'ContentType': content_type}
            )
        return name

    @cached_property
    def location(self):
        return self._location

    @cached_property
    def base_url(self):
        if self._base_url is not None and not self._base_url.endswith('/'):
            self._base_url += '/'
        return self._base_url

    def get_valid_name(self, name):
        form = "%Y-%m-%d"
        prefix = os.path.normpath(
            force_unicode(
                timezone.now().strftime(
                    smart_str(form)
                )
            )
        )
        return '%s%s/%s' % (self.location, prefix, name)

    def delete(self, name):
        # delete object
        client.delete_object(Bucket=s3_container, Key=name)

    def exists(self, name):
        return self.size(name) > 0

    def size(self, name):
        try:
            obj = client.get_object(Bucket=s3_container, Key=name)
        except:
            return 0
        return obj['ContentLength']

    def url(self, name):
        return "%s%s" % (self.base_url, name)